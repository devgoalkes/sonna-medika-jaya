@extends('layouts.front.template')
@section('title', $specialty->title)
@section('metatitle', $specialty->title)
@section('metadescription', $specialty->title)

@section('content')
<section class="banner-area " id="home">
  <div class="container">
    <div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">{{ $specialty->title }}</h2>
			</div>
		</div>
  </div>
</section>

<section class="products-area product-page pt-30">
	<div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-md-8 pb-40">
        <img class="img-fluid" src="{{ asset('storage/uploads/specialty/'. $specialty->picture ) }}" alt="">
        <h1 class="pb-20 pt-20">{{ $specialty->title }}</h1>
				{!! $specialty->content !!}
			</div>
      {{-- @include('layouts.front.partials._sidebar') --}}
		</div>

	</div>
</section>
@endsection
