let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
 mix.styles([
         'public/css/bootstrap.css',
         'public/css/animation.css',
         'public/css/fancybox.css',
				 'public/css/front-custom.css',
				 'public/css/font-awesome.min.css',
				 'public/css/style.css',
         'public/css/media.css',
         'public/css/jssocials.css',
         'public/css/jssocials-theme-classic.css'
      ], 'public/css/goalkes-help.min.css')
   .scripts([
         'public/js/jquery-2.2.4.min.js',
         'public/js/bootstrap-tabcollapse.js',
         'public/js/fastclick.js',
         'public/js/bootstrap.js',
         'public/js/js.cookie.js',
         'public/js/theme-setting.js',
         'public/js/jssocials.min.js'
      ], 'public/js/goalkes-help.min.js')
    .version();
