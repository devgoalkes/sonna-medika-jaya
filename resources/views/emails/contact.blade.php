@component('mail::message')
# Email From Customer

This is an email from the Customer that is forwarded from the website of PT. Sonna Medika Jaya. Please follow up immediately.

@component('mail::panel')
<b>{{ $subject }}</b><br>
<small style="font-size:10px;">{{ tglIndo($contact->created_at) }}</small>
@endcomponent

@component('mail::table')
|           |                          |
| --------- |:-------------------------|
| Name      | {{ $contact->name }}     |
| From      | {{ $contact->email }}    |
| Message   | {{ $contact->message }}  |
@endcomponent



<hr>

Thanks,<br>
<span style="font-size:10px;">{{ config('app.name') }}</span>
@endcomponent
