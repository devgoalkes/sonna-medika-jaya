<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
   	public $incrementing = false;

    public function socials()
    {
      return $this->hasMany(Social::class, 'id', 'identity_id');
    }
}
