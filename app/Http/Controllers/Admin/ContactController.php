<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;

use App\Models\Contact;
use Ramsey\Uuid\Uuid;
use DataTables;
use Session;
use Alert;

class ContactController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Contact::all())
                ->addColumn('link', function ($contact) {
                    return '
                    <div class="btn-group">
                    <button type="button" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".bs-example-modal-md" data-href="'. route('admin.contact.show', $contact) .'"><i class="fa fa-eye"></i></button>

                    <a href="'. route('admin.contact.edit', $contact) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                    <button type="button" onclick="konfirm(this)" data-id="'.$contact->id.'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </div>
                    ';
                })
                ->rawColumns(['link'])
                ->addIndexColumn()
                ->make(true);
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Nama', 'searchable'=> true],
            ['data' => 'email', 'name' => 'email', 'title' => 'Email', 'searchable'=> false],
            ['data' => 'subject', 'name' => 'subject', 'title' => 'Subject', 'searchable'=> true],
            ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
        ])->parameters([
            'stateSave' => 'true',
            'stateSaveCallback',
            'sPaginationType' => 'full_numbers'
        ]);

        return view('admin.contact.index', compact('html'));
    }

    public function show(Contact $contact)
    {
          $contact->read = '1';
          $contact->update();
          return view('admin.contact.show', compact('contact'));
    }

    public function countMessage()
    {
        $value = Contact::where('read', 0)->count();
        return $value;
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();
    }
}
