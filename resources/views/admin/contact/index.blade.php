@extends('layouts.admin.template')
@section('title', 'Contact')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li class="active">Inbox</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">&nbsp;</div>
	<div class="padding-md clearfix">
		<div class="row">
			<div class="col-md-12">
				{!! $html->table(['class' => 'table table-striped']) !!}
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-body">
        <div id="modalcontent"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('asset_admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('asset_admin/js/sweetalert.min.js') }}"></script>

<script>
		var id = $('.btn-detail').attr('data-href');
		console.log(id);
		$('.btn-detail').on('click', function(){
				// var id = $(this).attr('data-href');
				// console.log(id);
				$('#modalcontent').load(id);
		});

		function konfirm(id){
				var id = id.getAttribute('data-id');
				swal({
						title: "Are you sure?",
						text: "You will not be able to recover this data file!",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, delete it!",
						closeOnConfirm: false,
						showLoaderOnConfirm: true
				},
				function(){
						$.ajax({
								type: "POST",
								url: '{{ route('admin.contact.index') }}/' + id,
								data: {
										'_token': "{{ csrf_token() }}",
										'_method' : 'DELETE'
								},
								success: function() {
										$('#dataTableBuilder').DataTable().ajax.reload();
										swal("Deleted!", "Pesan Berhasil Dihapus!", "success");
								},
								error: function() {
										swal("Error!", "Pesan Gagal Dihapus!", "error");
								}
						});
				});
		}
</script>
{!! $html->scripts() !!}
@endsection
