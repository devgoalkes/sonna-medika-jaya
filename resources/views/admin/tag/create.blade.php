@extends('layouts.admin.template')
@section('title', 'Add Tags')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li></i><a onclick="location.href='{{ url('staticpages') }}'"> Static Pages</a></li>
		 <li class="active">Add</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Add Tags</div>
	<div class="panel-body">
		{!! Form::open(array('route' => 'admin.tag.store', 'methode' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) !!}
		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Tag Name</label>
			<div class="col-sm-10">
				<input type="text" name="name" class="form-control" id="name" placeholder="Tag Name" autocomplete="off">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('admin.tag.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/jquery.gritter.min.js') }}"></script>
@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif

@endsection
