<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{ Category, Event };

class EventController extends Controller
{
    public function index()
    {
        $events = Event::where('active', 'Y')->orderBy('created_at', 'desc')->get();
        return view('front.event.index', compact('events'));
    }

    public function detail(Event $event)
		{
        return view('front.event.detail', compact(['event']));
		}

    public function search(Request $request)
    {
        $events = Event::where('title', 'LIKE', "%$request->q%")->paginate('10');
        return view('front.event.search', compact('events'));
    }
}
