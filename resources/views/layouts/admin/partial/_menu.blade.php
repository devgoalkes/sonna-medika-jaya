<div class="main-menu">
  <ul>
    <?php
      $menu_0 = \App\Models\Menu::where('id_parent',0)->where('active', 'Y')->where('admin', 'Y')->get();
      foreach ($menu_0 as $key) {
        get_menu_child($key->id);
      }

      function get_menu_child($parent=0){
        $menu = \App\Models\Menu::where('id_parent',$parent)->get();
        $parent = \App\Models\Menu::where('id',$parent)->first();
        if(sizeof($menu) > 0){
        ?>
        <li class="openable">
          <a href="{{ url($parent->url) }}">
            <span class="menu-icon"><i class="fa {{ $parent->icon }} fa-lg"></i></span>
            <span class="text">{{ $parent->name }}  </span>
            <span class="badge badge-success bounceIn">{{ sizeof($menu) }}</span>
            <span class="menu-hover"></span>
          </a>
          <ul class="submenu">
            <?php foreach($menu as $key) { ?>
              <li><a href="{{ url($key->url) }}"><span class="submenu-label">{{ $key->name }}</span></a></li>
            <?php } ?>
          </ul>
        </li>
        <?php }else{ ?>
        <li class="{{ Request::is(url($parent->url)) ? 'active' : '' }}">
          <a href="{{ url($parent->url) }}">
            <span class="menu-icon"><i class="fa {{ $parent->icon }} fa-lg"></i></span>
            <span class="text">{{ $parent->name }}</span>
            <span class="menu-hover"></span>
          </a>
        </li>
        <?php } ?>
      <?php } ?>
      <li class="">
        <a href="{{ route('admin.menu.index') }}">
          <span class="menu-icon"><i class="fa fa-bars fa-lg"></i></span>
          <span class="text">Menu</span>
          <span class="menu-hover"></span>
        </a>
      </li>
  </ul>

</div>
