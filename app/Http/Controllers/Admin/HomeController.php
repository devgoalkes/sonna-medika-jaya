<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ User, Article };

class HomeController extends Controller
{
    public function index()
    {
        $users    = User::count();
        $articles    = Article::count();
        return view('admin.home', compact('users', 'articles'));
    }
}
