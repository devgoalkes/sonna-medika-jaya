<!DOCTYPE html>
<html lang="id" class="no-js">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" href="img/fav.html">
		<meta name="author" content="codepixer">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta charset="UTF-8">
		<link rel="icon" type="image/gif" href="{{ asset('images/favicon.gif') }}">
		<title>PT. Sonna Medika Jaya</title>

    @include('layouts.front.partials._style')
		</head>
		<body>
			 @include('layouts.front.partials._header')

			@yield('content')

			@include('layouts.front.partials._footer')
			<a target="_blank" href="https://api.whatsapp.com/send?phone=628176870687&text=Halo%20Marketing%20Support" class="button btn-fx6 hidden-lg hidden-md top"><img src="{{ asset('images/wab.png') }}" alt="" width="25px" style="padding-top:8px;"></a>
			<a target="_blank" href="https://api.whatsapp.com/send?phone=628119258881&text=Halo%20Marketing%20Support" class="button btn-fx6 hidden-lg hidden-md bottom"><i class="fa fa-whatsapp" style="padding-top:8px;"></i></a>

			<a target="_blank" href="https://web.whatsapp.com/send?phone=628176870687&text=Halo%20Marketing%20Support" class="button btn-fx6 hidden-sm hidden-xs top"><img src="{{ asset('images/wab.png') }}" style="padding-top:6px;"></a>
			<a target="_blank" href="https://web.whatsapp.com/send?phone=628119258881&text=Halo%20Marketing%20Support" class="button btn-fx6 hidden-sm hidden-xs bottom"><i class="fa fa-whatsapp" style="padding-top:6px;"></i></a>
      @include('layouts.front.partials._script')
		</body>
</html>
