@extends('layouts.front.template')
@section('title', "Category ". $category->name)
@section('metatitle', "Category ". $category->name)
@section('metadescription', "Category ". $category->name)
@section('image', '')

@section('content')
<section class="products-area product-page pt-120">
	<div class="container">
    <div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">{{ $category->name }}</h2>
				<h4 class="pb-10">Our Specialties</h4>
			</div>
		</div>

		<div class="row pb-80">
			@if($category->subcategory->count() > 0)
					@foreach($category->subcategory as $cat)
							@forelse($cat->specialties as $specialty)
							<div class="col-lg-3">
								<div class="single-offered-service">
									<img class="img-fluid" src="{{ asset('storage/uploads/specialty/'. $specialty->picture) }}" alt="{{ $specialty->name }}">
									<a href="{{ route('front.specialty.detail', [$specialty->category,$specialty]) }}">
										<h4>{{ $specialty->title }}</h4>
									</a>
									<p>
										{!! str_limit(strip_tags($specialty->content), 100) !!}
									</p>
								</div>
							</div>
							@endforeach
					@endforeach
			@else
					@forelse($category->specialties as $specialty)
					<div class="col-lg-3">
						<div class="single-offered-service">
							<img class="img-fluid" src="{{ asset('storage/uploads/specialty/'. $specialty->picture) }}" alt="{{ $specialty->name }}">
							<a href="{{ route('front.specialty.detail', [$specialty->category,$specialty]) }}">
								<h4>{{ $specialty->title }}</h4>
							</a>
							<p>
								{!! str_limit(strip_tags($specialty->content), 100) !!}
							</p>
						</div>
					</div>
					@empty
		      <div class="col-md-12 margin_bottom30">
		        <div class="panel panel-default">
		          <div class="panel-heading text-center">
		            <h3>Sorry there is no product in the category {{ $category->name }}.</h3>
		          </div>
		        </div>
		      </div>
					@endforelse
			@endif
		</div>
	</div>
</section>
@endsection

@section('styles')
<style media="screen">
  .blog-row {margin-top:50px; }
  .margin_bottom30 { margin-bottom:30px; }
  .margin_left10 {margin-left:10px; }
  .blog-content {padding:10px; }
  .bg-white {background-color:#fff;}
</style>
@endsection
