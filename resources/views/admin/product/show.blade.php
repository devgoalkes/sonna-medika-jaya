<div class="panel blog-container">
	<div class="panel-body">
		<h4>{{ $product->name }}</h4>
		<small class="text-muted">Tanggal : {{ tglIndo($product->created_at) }} </small>
		<div class="seperator"></div>
    <div class="input-group">
				<input id="slug" type="text" value="{{ 'product/'.$product->category->slug.'/'.$product->slug }}" class="form-control">
				<span class="input-group-btn">
					<button class="btn btn-success copy-button" data-clipboard-action="copy" data-clipboard-target="#slug">Copy</button>
				</span>
			</div>
		<div class="seperator"></div>
		@if($product->picture && $product->picture != '-' )
		<img width="200px" class="img-thumbnail" style="float:left; margin-right:10px" src="{{ url('storage/uploads/products/'.$product->picture) }}" alt="{{ $product->name }}">
		@endif

		{!! $product->description !!}

	</div>
</div>
