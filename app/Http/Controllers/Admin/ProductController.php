<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Str;

use App\Models\{Product, Category};
use Ramsey\Uuid\Uuid;
use Session;
use DataTables;
use Alert;

class ProductController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Product::get())
                ->addColumn('link', function ($product) {
                    return '
                    <div class="btn-group">
                    <button type="button" onclick="show(this)" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".modal-detail" data-href="'. route('admin.product.show', $product->id) .'"><i class="fa fa-eye"></i></button> <a href="'. route('admin.product.edit', $product->id) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a> <button type="button" onclick="konfirm(this)" data-href="'. route('admin.product.destroy', $product->id) .'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </div>
                    ';
                })
                ->addColumn('created_at', function ($product) {
                    return tglWaktuIndo($product->created_at, false);
                })
                ->addColumn('category', function ($product) {
                    return $product->category->name;
                })
                ->rawColumns(['link'])
                ->addIndexColumn()
                ->make(true);
        }

        $html = $builder->columns([
            ['data' => 'DT_Row_Index', 'name' => 'id', 'title' => 'No', 'searchable'=> false, 'orderable' => false],
            ['data' => 'name', 'name' => 'name', 'title' => 'Nama', 'searchable'=> true],
            ['data' => 'category', 'name' => 'category', 'title' => 'Kategori', 'searchable'=> true],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tanggal', 'searchable'=> true],
            ['data' => 'active', 'name' => 'active', 'title' => 'Active', 'searchable'=> false],
            ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
        ])->parameters([
            'stateSave' => 'true',
            'stateSaveCallback',
            'sPaginationType' => 'full_numbers'
        ]);
        return view('admin.product.index', compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('active', 'Y')->where('slug', 'products')->first();
        return view('admin.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
      $product = new Product;

      if ($request->hasFile('picture')) {
          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/products'), $photoName);
          $product->picture = $photoName;
      }else{
          $product->picture = '-';
      }

      $product->id          = Uuid::uuid4()->toString();
      $product->category_id = $request->category_id;
      $product->name        = $request->name;
      $product->brand       = $request->brand;
      $product->slug        = Str::slug($request->name .'-'. rand(1,99));
      $product->description = $request->description;
      $product->active      = $request->active;
      $product->created_at  = date('Y-m-d H:i:s');

      if ($product->save()) {
          Alert::success('Data berhasil ditambah !', 'Sukses');
          return redirect()->route('admin.product.index');
       }else{
          Alert::error('Silahkan di coba lagi !', 'Error');
          return redirect()->route('admin.product.index');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('id', $id)->first();

        return view('admin.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::where('id', $id)->first();

        $categories = Category::where('active', 'Y')->where('slug', 'products')->first();
        return view('admin.product.edit', compact('product', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::where('id', $id)->first();

        if ($request->hasFile('picture')) {
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/products'), $photoName);
            $product->picture = $photoName;
        }else{
            $product->picture = $request->picture_old;
        }

        $product->name 		  = $request->name;
        $product->category_id = $request->category_id;
        $product->slug 		  = Str::slug($request->name .'-'. rand(1,99));
        $product->description = $request->description;
        $product->active 	  = $request->active;

        if ($product->save()) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.product.index');
        }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.product.edit', $product);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::where('id', $id)->first();

        if ($product->delete()){
            Alert::success('Data berhasil dihapus', 'Sukses');
            return redirect()->route('admin.product.index');
        }else{
            Alert::error('Data tidak berhasil dihapus', 'Error');
            return redirect()->route('admin.product.index');
        }
    }
}
