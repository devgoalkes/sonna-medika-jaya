<footer class="footer-area">
  <div class="container">

    <div class="row footer-bottom d-flex justify-content-between">
      <p class="col-lg-8 col-sm-12 footer-text m-0 text-white">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | PT. Sonna Medika Jaya</a>
    </p>
      <div class="col-lg-4 col-sm-12 footer-social">
        @foreach($socials as $social)
        @if($social->driver == 'instagram')
        <a href="https://www.instagram.com/{{ $social->username }}" target="_blank"><i class="fa fa-{{ $social->driver }}"></i></a>
        @endif
        @if($social->driver == 'twitter')
        <a href="https://twitter.com/{{ $social->username }}" target="_blank"><i class="fa fa-{{ $social->driver }}"></i></a>
        @endif
        @if($social->driver == 'facebook')
        <a href="https://www.facebook.com/{{ $social->username }}" target="_blank"><i class="fa fa-{{ $social->driver }}"></i></a>
        @endif
        @endforeach
        <a href="https://www.linkedin.com/in/%E5%BE%90-handyono-bb550764/" target="_blank"><i class="fa fa-linkedin"></i></a>
      </div>
    </div>
  </div>
</footer>
