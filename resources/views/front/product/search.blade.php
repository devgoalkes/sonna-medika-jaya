@extends('layouts.front.template')
@section('metatitle', "Pencarian")
@section('metadescription', "Pencarian")
@section('title', 'Pencarian')

@section('content')

<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
            </div>
        </div>
    </div>
</section>

<section class="products-area product-page pt-40">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">Our Products</h2>
        @if($products->count() > 0)
				<p>
				Found <b>{{ $products->count() }}</b> events with the keywords "{{ request()->q }}".
				</p>
        @endif
			</div>
		</div>
		<div class="row justify-content-md-center">
      @forelse($products as $product)
			<div class="col-lg-3 col-md-6">
				<div class="single-product">
					<div class="thumb">
						<img src="{{ asset('storage/uploads/products/'. $product->picture) }}" alt="{{ $product->name }}" width="100%" height="150px">
					</div>
					<div class="details">
						<h4>{!! str_limit(strip_tags($product->name), 20) !!}</h4>
						<p>
							{!! str_limit(strip_tags($product->description), 100) !!}
						</p>
						<a href="{{ route('front.product.detail', [$product->category,$product]) }}" class="primary-btn text-uppercase">View Details</a>
					</div>
				</div>
			</div>
			@empty

      <div class="col-md-12 pb-30 text-center">
        <h4>Sorry, the product with the keyword "{{ request()->q }}" was not found.</h4>
      </div>

      <div class="col-md-6 pb-40">
        <form method="get" class="form-horizontal" action="{{ route('front.product.search') }}">
          {{ csrf_field() }}
          <div class="input-group-icon mt-10">
            <input type="text" name="q" placeholder="Search our products" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search our products'" required class="single-input-primary search-input">
            <button type="submit" class="icon"><i class="fa fa-search fa-lg" aria-hidden="true"></i></button>
          </div>
        </form>
      </div>
      @endforelse
		</div>
	</div>
</section>

@endsection
