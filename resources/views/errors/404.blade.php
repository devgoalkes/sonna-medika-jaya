@extends('layouts.front.template')
@section('title', "404 Not Found")
@section('metatitle', "404 Not Found")
@section('metadescription', "404 Not Found")
@section('content')
<section class="blog-posts-area pt-120 mt-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 post-list blog-post-list text-center">
				<div class="single-post">
					<a href="#">
						<h2>404!</h2>
					</a>
          <hr>
					<div class="content-wrap">

            <h5>404 Not Found</h5>
            <p>Sorry :( , we couldn't find this page.</p>
            <a href="{{ url('/') }}" class="default-btn">Back to Home</a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
