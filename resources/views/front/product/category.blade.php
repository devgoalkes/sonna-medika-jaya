@extends('layouts.front.template')
@section('title', "Category ". $category->name)
@section('metatitle', "Category ". $category->name)
@section('metadescription', "Category ". $category->name)
@section('image', '')

@section('content')
<section class="products-area product-page pt-120">
	<div class="container">
    <div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">{{ $category->name }}</h2>
			</div>
		</div>
		<div class="row justify-content-center">
			@foreach($category->subcategory as $subcategory)
			<div class="col-md-3 pb-10 ">
					<a href="{{ route('front.product.category', $subcategory) }}" class="primary-btn">{{ $subcategory->name }}</a>
			</div>
			@endforeach
		</div>

    <div class="row">
      @forelse($products as $product)
      <div class="col-lg-3 col-md-6">
        <div class="single-product">
          <div class="thumb">
            <img src="{{ asset('storage/uploads/products/'. $product->picture) }}" alt="{{ $product->name }}" width="100%" height="150px">
          </div>
          <div class="details">
            <h4>{{ $product->name }}</h4>
            <p>
              {!! str_limit(strip_tags($product->description), 50) !!}
            </p>
            <a href="{{ route('front.product.detail', [$product->category,$product]) }}" class="primary-btn text-uppercase">View Details</a>
          </div>
        </div>
      </div>
      @empty
      <div class="col-md-12 pb-80" style="text-align: center; ">
				<h3>There are no products in this category.</h3>
			</div>
      @endforelse
		</div>
	</div>
</section>
@endsection

@section('styles')
<style media="screen">
  .blog-row {margin-top:50px; }
  .margin_bottom30 { margin-bottom:30px; }
  .margin_left10 {margin-left:10px; }
  .blog-content {padding:10px; }
  .bg-white {background-color:#fff;}
</style>
@endsection
