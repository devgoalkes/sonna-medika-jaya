@extends('layouts.front.template')
@section('content')
@section('metatitle', "Partnership")
@section('metadescription', "Partnership")
@section('front_title', "Partnership")

@section('content')
<section class="contact-page-area section-gap">
	<div class="container">
		<div class="row pb-20">
			<div class="col-md-12">
				<a href="#">
	        <h2 class="pb-10">
	          Partnership
	        </h2>
	      </a>
			</div>
		</div>
		<div class="row">

			<div class="col-lg-4 d-flex flex-column address-wrap">
				<div class="single-contact-address d-flex flex-row">
					<div class="icon">
						<span class="lnr lnr-home"></span>
					</div>
					<div class="contact-details">
						<h5>{{ $setting->address }}</h5>
					</div>
				</div>
				<div class="single-contact-address d-flex flex-row">
					<div class="icon">
						<span class="lnr lnr-phone-handset"></span>
					</div>
					<div class="contact-details">
						<h5>{{ $setting->phone }}</h5>
            <p>Mon to Fri 9am to 6 pm</p>
					</div>
				</div>
				<div class="single-contact-address d-flex flex-row">
					<div class="icon">
						<span class="lnr lnr-envelope"></span>
					</div>
					<div class="contact-details">
						<h5>{{ $setting->mail_address }}</h5>
						<p>Send us your query anytime!</p>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
        <div id="msgSubmit"></div>
        <div id="loading"></div>
				<form class="form-area " id="contactform" class="contact-form text-right">
					<div class="row">
						<div class="col-lg-6 form-group">
							<input name="contact_name" id="name" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mb-20 form-control" required="" type="text" autocomplete="off">

							<input name="contact_email" id="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mb-20 form-control" required="" type="email" autocomplete="off">

							<!-- <input name="contact_subject" id="subject" placeholder="Enter your subject" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your subject'" class="common-input mb-20 form-control" required="" type="text" autocomplete="off"> -->

              <div class="mb-20 form-control">
								<div class="default-select" id="subject">
									<select id="message_subject">
                    <option value="0">Select Subject</option>
										<option value="1">Demo Request</option>
										<option value="2">Distributor Over</option>
                    <!-- <option value="3">Lainnya</option> -->
									</select>
								</div>
							</div>

						</div>
						<div class="col-lg-6 form-group">
							<textarea class="common-textarea form-control" name="contact_message" id="message" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
						</div>
						<div class="col-lg-12 d-flex justify-content-between">
							<div class="alert-msg" style="text-align: left;"></div>
							<button class="genric-btn primary circle" style="float: right;">Send Message</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6J-39nNLYLNSDbTuZIrdJgRzd9ZKpClE"></script>
<script src="{{ asset('front/contact-form/js/validator.min.js') }}"></script>
<script src="{{ asset('front/js/share.js') }}"></script>
<script>

	jQuery('.df-share, .df-tweet, .df-pin, .df-pluss, .df-link').click(function(event) {
		var width  = 575,
			height = 400,
			left   = (jQuery(window).width()  - width)  / 2,
			top    = (jQuery(window).height() - height) / 2,
			url    = this.href,
			opts   = 'status=1' +
					 ',width='  + width  +
					 ',height=' + height +
					 ',top='    + top    +
					 ',left='   + left;

		window.open(url, 'twitter', opts);

		return false;
	});

	var TWEET_URL = "https://twitter.com/intent/tweet";

	jQuery(".df-tweet").each(function() {
		var elem = jQuery(this),
		// Use current page URL as default link
		url = encodeURIComponent(elem.attr("data-url") || document.location.href),
		// Use page title as default tweet message
		text = elem.attr("data-text") || document.title,
		via = elem.attr("data-via") || "",
		related = encodeURIComponent(elem.attr("data-related")) || "",
		hashtags = encodeURIComponent(elem.attr("data-hashtags")) || "";

		// Set href to tweet page
		elem.attr({
			href: TWEET_URL + "?original_referer=" +
					encodeURIComponent(document.location.href) +
					"&source=tweetbutton&text=" + text + "&url=" + url + "&via=" + via + "&hashtags=" + hashtags,
			target: "_blank"
		});

	});
</script>
<script>
$("#contactform").validator().on("submit", function (event) {
    $('#loading').html('<h5 data-text="Loading...">Loading...</h5>');
    if (event.isDefaultPrevented()) {
        formError();
        submitMSG(false, "Silahkan isi form dengan benar.");
    } else {
        event.preventDefault();
        submitForm();
    }
});

function submitForm(){
    var name = $("#name").val();
    var email = $("#email").val();
    var subject = $("#message_subject").val();
    var message = $("#message").val();

    $.ajax({
        type: "POST",
        url: "{{ route('front.contact') }}",
        data: "contact_name=" + name + "&contact_email=" + email + "&contact_subject=" + subject + "&contact_message=" + message + "&_token={{ csrf_token() }}",
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#contactform")[0].reset();
    submitMSG(true, "Pesan anda telah terkirim !");
}

function formError(){
    $("#contactform").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h5 alert alert-success";
    }else {
        var msgClasses = "h5 alert alert-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
    $('#loading').html('');
}
</script>
@endpush
