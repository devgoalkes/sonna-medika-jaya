@extends('layouts.admin.template')
@section('title', 'Add Settings')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ route('admin.setting.index') }}'">Identity</a></li>
		 <li class="active">Add</li>
	</ul>
</div>
@endsection
@section('content')

<section class="panel panel-primary">
	<header class="panel-heading">Add Identity Data</header>
	<div class="panel-body">
	{!! Form::open(array('route' => 'admin.setting.store', 'method' => 'POST', 'class' => 'form-horizontal')) !!}

		<div class="form-group">
			<label for="website_name" class="col-sm-2 control-label">Website Name</label>
			<div class="col-sm-10">
				<input type="text" name="website_name" class="form-control" id="website_name" placeholder="Website Name" value="" autocomplete="off">
			</div>
		</div>
    <div class="form-group">
      <label for="website_address" class="col-sm-2 control-label">Website Address</label>
      <div class="col-sm-10">
        <input type="text" name="website_address" class="form-control" id="website_address" placeholder="Website Address" value="" autocomplete="off">
      </div>
    </div>
    <div class="form-group">
      <label for="meta_description" class="col-sm-2 control-label">Meta Description</label>
      <div class="col-sm-10">
        <input type="text" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description" value="" autocomplete="off">
      </div>
    </div>
    <div class="form-group">
      <label for="meta_keyword" class="col-sm-2 control-label">Meta Keyword</label>
      <div class="col-sm-10">
        <input type="text" name="meta_keyword" class="form-control" id="meta_keyword" placeholder="Meta Keyword" value="" autocomplete="off">
      </div>
    </div>
    <div class="form-group">
      <label for="favicon" class="col-sm-2 control-label">Favicon</label>
      <div class="col-sm-10">
        <input type="text" name="favicon" class="form-control" id="favicon" placeholder="Favicon" value="" autocomplete="off">
      </div>
    </div>
		<div class="form-group">
			<label for="address" class="col-sm-2 control-label">Office Address</label>
			<div class="col-sm-10">
				<textarea id="address" name="address" placeholder="Enter your text ..." class="form-control" rows="2"></textarea>
			</div>
		</div>
    <div class="form-group">
      <label for="mail_address" class="col-sm-2 control-label">Mail Address</label>
      <div class="col-sm-10">
        <input type="text" name="mail_address" class="form-control" id="mail_address" placeholder="Mail Address" value="" autocomplete="off">
      </div>
    </div>
    <div class="form-group">
      <label for="phone" class="col-sm-2 control-label">Phone</label>
      <div class="col-sm-10">
        <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone Number" value="" autocomplete="off">
      </div>
    </div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('admin.setting.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</section>
@endsection


@section('style')
<link href="{{ asset('asset_admin/css/chosen/chosen.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('asset_admin/js/ckeditor/content.css') }}" rel="stylesheet"/>
<link href="{{ asset('asset_admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('asset_admin/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('asset_admin/js/ckeditor/script/sample.js') }}"></script>
<script src="{{ asset('asset_admin/js/jquery.gritter.min.js') }}"></script>
<script>
	$(".chzn-select").chosen();
	initSample();
</script>
@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif
@endsection
