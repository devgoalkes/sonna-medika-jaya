<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use Alert;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::find(1);
        if($setting){
          return view('admin.setting.index', compact('setting'));
        }else{
          return redirect()->route('admin.setting.create');
        }

    }

    public function create()
    {
        return view('admin.setting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['website_name'] 	  = $request->website_name;
        $data['website_address']  = $request->website_address;
        $data['meta_description'] = $request->meta_description;
        $data['meta_keyword'] 		= $request->meta_keyword;
        $data['favicon'] 			    = $request->favicon;
        $data['mail_address'] 		= $request->mail_address;
        $data['address'] 			    = $request->address;
        $data['phone'] 			      = $request->phone;

        if (Setting::insert($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.setting.index');
        }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.setting.create');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        return view('admin.setting.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
         $data['website_name'] 	  = $request->website_name;
         $data['website_address'] = $request->website_address;
         $data['meta_description']= $request->meta_description;
         $data['meta_keyword'] 		= $request->meta_keyword;
         $data['favicon'] 			  = $request->favicon;
         $data['mail_address'] 		= $request->mail_address;
         $data['address'] 			  = $request->address;
         $data['phone'] 			    = $request->phone;

         if (Setting::where('id', $id)->update($data)) {
             Alert::success('Data berhasil disimpan', 'Sukses');
             return redirect()->route('admin.setting.index');
         }else{
             Alert::error('Silahkan di coba lagi !', 'Error');
             return redirect()->route('admin.setting.edit', $id);
         }
     }
}
