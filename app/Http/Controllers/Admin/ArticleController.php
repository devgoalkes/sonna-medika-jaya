<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use Yajra\DataTables\Html\Builder;

use App\Models\{ Article, Category };
use Ramsey\Uuid\Uuid;
use DataTables, Alert, Storage;

class ArticleController extends Controller
{

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Article::get())
                ->addColumn('link', function ($article) {
                    return '
                    <div class="btn-group">
                    <button type="button" onclick="show(this)" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".modal-detail" data-href="'. route('admin.article.show', $article) .'"><i class="fa fa-eye"></i></button>
                    <a href="'. route('admin.article.edit', $article) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                    <button type="button" onclick="konfirm(this)" data-href="'. route('admin.article.destroy', $article) .'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </div>
                    ';
                })
                ->addColumn('created_at', function ($article) {
                    return tglWaktuIndo($article->created_at);
                })
                ->rawColumns(['link'])
                ->addIndexColumn()
                ->make(true);
        }

        $html = $builder->columns([
            ['data' => 'DT_Row_Index', 'name' => 'id', 'title' => 'No', 'searchable'=> false, 'orderable' => false],
            ['data' => 'title', 'name' => 'title', 'title' => 'Judul', 'searchable'=> true],
            ['data' => 'active', 'name' => 'active', 'title' => 'Aktif', 'searchable'=> false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tanggal', 'searchable'=> true],
            ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
        ])->parameters([
            'stateSave' => 'true',
            'stateSaveCallback',
            'sPaginationType' => 'full_numbers'
        ]);
        return view('admin.article.index', compact('html'));
    }

    public function create()
    {
        $category = Category::where('active', 'Y')->where('slug', 'article')->first();
        return view('admin.article.create', compact('category'));
    }

    public function store(ArticleRequest $request)
    {
        $article = new Article;

        if ($request->hasFile('picture')) {
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/articles'), $photoName);
            $article->picture    = $photoName;
        }
        $article->id         = Uuid::uuid4()->toString();
        $article->title      = $request->title;
        $article->slug       = str_slug($request->title);
        $article->content    = $request->content;
        $article->active     = $request->active;
        $article->created_at = date('Y-m-d H:i:s');

        if ($article->save()) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.article.index');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.create.index');
         }
    }

    public function show(Article $article)
    {
        return view('admin.article.show', compact('article'));
    }

    public function edit(Article $article)
    {
        return view('admin.article.edit', compact('article'));
    }

    public function update(ArticleRequest $request, Article $article)
    {
        if ($request->hasFile('picture')) {
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/articles'), $photoName);

            $article->picture      = $photoName;
        }else{
            $article->picture      = $request->picture_old;
        }
        $article->title        = $request->title;
        $article->slug         = str_slug($request->title);
        $article->content      = $request->content;
        $article->active       = $request->active;
        $article->updated_at   = date('Y-m-d H:i:s');

        $article->save();
        Alert::success('Data berhasil disimpan', 'Sukses');
        return redirect()->route('admin.article.index');
    }

    public function destroy(Article $article)
    {
        if ($article->delete()){
            Storage::delete('upload/articles/'.$article->picture);
        }
    }
}
