
	@yield('breadcrumb')
	<div class="main-header">
		<span style="float:right">@yield('button')</span>
		<h4 style="float:left">@yield('title')</h4>
	</div><!-- /main-header -->
