<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class ShowArticleCategory extends Controller
{
    public function __invoke(Category $category, Request $request)
    {
        return view('front.category', compact('category'));
    }
}
