<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Social;

class Socials
{

  	public $socials;


  	function __construct(Social $socials)
  	{
  		$this->socials = Social::all();
  	}

  	public function compose(View $view)
  	{
  		$view->with('socials', $this->socials);
  	}
}
