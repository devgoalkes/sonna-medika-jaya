@extends('layouts.admin.template')
@section('title', 'Edit Menu')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ route('admin.menu.index') }}'">Menu</a></li>
		 <li class="active">Edit</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Edit Menu</div>
	<div class="panel-body">
		{!! Form::open(array('route' => ['admin.menu.update', $menu], 'method' => 'PUT', 'class' => 'form-horizontal')) !!}
		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Name</label>
			<div class="col-sm-5">
				<input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ $menu->name }}">
			</div>
		</div>

		<div class="form-group">
			<label for="id_parent" class="col-sm-2 control-label">Parent ID</label>
			<div class="col-sm-5">
				<select class="form-control error chzn-select" name="id_parent">
						<option value="0" selected>No Parent</option>
						@if(!$parent_menus->isEmpty())
							@foreach($parent_menus as $r)
								@if($r->id == $menu->id_parent)
									<option value="{{ $r->id }}" selected>{{ $r->name }}</option>
								@else
									<option value="{{ $r->id }}">{{ $r->name }}</option>
								@endif
							@endforeach
						@else
						<option value="0">No Parent</option>
					@endif
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="url" class="col-sm-2 control-label">URL</label>
			<div class="col-sm-5">
				<input type="text" name="url" class="form-control" placeholder="URL" value="{{ $menu->url }}">
			</div>
		</div>
		<div class="form-group">
			<label for="icon" class="col-sm-2 control-label">Icon</label>
			<div class="col-sm-5">
					<input type="text" name="icon" class="form-control icon_menu" id="icon_menu" placeholder="Icon" value="{{ $menu->icon }}">
			</div>
		</div>
		<div class="form-group">
			<label for="admin" class="col-sm-2 control-label">Admin</label>
			<div class="col-lg-10">
				@if($menu->admin == 'Y')
				<label class="label-radio inline">
					<input type="radio" name="admin" value="Y" checked>
					<span class="custom-radio"></span>
					Yes
				</label>
				<label class="label-radio inline">
					<input type="radio" name="admin" value="N">
					<span class="custom-radio"></span>
					No
				</label>
				@else
				<label class="label-radio inline">
					<input type="radio" name="admin" value="Y">
					<span class="custom-radio"></span>
					Yes
				</label>
				<label class="label-radio inline">
					<input type="radio" name="admin" value="N" checked>
					<span class="custom-radio"></span>
					No
				</label>
				@endif
			</div>
		</div>

		<div class="form-group">
			<label for="active" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				@if($menu->active == 'Y')
				<label class="label-radio inline">
					<input type="radio" name="active" value="Y" checked>
					<span class="custom-radio"></span>
					Aktive
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
				@else
				<label class="label-radio inline">
					<input type="radio" name="active" value="Y">
					<span class="custom-radio"></span>
					Aktive
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" value="N" checked>
					<span class="custom-radio"></span>
					Non Active
				</label>

				@endif
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('admin.menu.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>

		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/fontawesome-iconpicker.css') }}" rel="stylesheet">
<link href="{{ asset('asset_admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('asset_admin/js/fontawesome-iconpicker.js') }}"></script>
<script>
	$('.icon_menu').iconpicker();
	$(".chzn-select").chosen();
</script>

@endsection
