<div class="panel blog-container">
	<div class="panel-body">
		<h4>{{ $page->title }}</h4>
		<small class="text-muted">Tanggal : {{ tglIndo($page->created_at) }} </small>
		<div class="seperator"></div>
    <div class="input-group">
				<input id="slug" type="text" value="{{ 'pages/'.$page->slug }}" class="form-control">
				<span class="input-group-btn">
					<button class="btn btn-success copy-button" data-clipboard-action="copy" data-clipboard-target="#slug">Copy</button>
				</span>
			</div>
		<div class="seperator"></div>
		@if( $page->picture != '' )
		<img width="200px" class="img-thumbnail" style="float:left; margin-right:10px" src="{{ url('storage/uploads/page/'.$page->picture) }}" alt="{{ $page->title }}">
		@endif
		{!! $page->content !!}

	</div>
</div>
