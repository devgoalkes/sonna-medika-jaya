<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  @include('layouts.admin.partial._head')

  <body class="overflow-hidden">
	<div id="overlay" class="transparent"></div>

	<div id="wrapper" class="preload">
		@include('layouts.admin.partial._topNav')

		<aside class="default">
			<div class="sidebar-inner scrollable-sidebar">
				<div class="size-toggle">
					<a class="btn btn-sm" id="sizeToggle">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
						<i class="fa fa-power-off"></i>
					</a>
				</div>
				<div class="user-block clearfix">
					<img src="{{ asset('asset_admin/img/unknown.png') }}" alt="User Avatar">
					<div class="detail">
						<strong>{{ Auth::user()->name }}</strong><span class="countMessage"></span>
						<ul class="list-inline">
							<li><a href="{{ route('admin.profil.index') }}">Profile</a></li>
						</ul>
					</div>
				</div>

				@include('layouts.admin.partial._menu')
			</div>
		</aside>

		<div id="main-container">
			<div>@include('layouts.admin.title')</div>
			<div class="padding-md">
				<div class="row">
					@yield('content')
				</div>
			</div>
		</div>
    </div>
		@include('layouts.admin.partial._footer')

	@yield('js')
	@include('sweet::alert')
  </body>
</html>
