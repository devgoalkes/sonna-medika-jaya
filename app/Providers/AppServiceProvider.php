<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // View::composer('layouts.front.partials._footer', 'App\Http\ViewComposers\Latest');
        View::composer('layouts.front.partials._navigation', 'App\Http\ViewComposers\Menus');
        View::composer('layouts.front.partials.mobile._navigation', 'App\Http\ViewComposers\Menus');
        View::composer('layouts.front.partials._sidebar', 'App\Http\ViewComposers\Latest');
        View::composer('layouts.front.partials._sidebar', 'App\Http\ViewComposers\Categories');
        View::composer('layouts.front.partials._footer', 'App\Http\ViewComposers\Socials');

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path('Helpers/ItjadHelper.php');
    }
}
