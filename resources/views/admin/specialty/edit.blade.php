@extends('layouts.admin.template')
@section('title', 'Edit Specialty')
@section('content')

<section class="panel panel-primary">
	<header class="panel-heading">Edit Specialty</header>
	<div class="panel-body">

	{!! Form::open(['route' => ['admin.specialty.update', $specialty], 'method' => 'PUT', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}

		<div class="form-group">
			<label for="title" class="col-sm-2 control-label">Specialty Title</label>
			<div class="col-sm-10">
				<input type="text" name="title" class="form-control" id="title" placeholder="Judul Halaman" value="{{ $specialty->title }}">
			</div>
		</div>

		<div class="form-group">
			<label for="category_id" class="col-sm-2 control-label">Category</label>
			<div class="col-sm-4">
				<select class="form-control" name="category_id">
					<option value="">No Category</option>
					@foreach($category->subcategory as $cat)
						@if($specialty->category_id == $cat->id)
						<option value="{!! $cat->id !!}" selected>{{ $cat->name }}</option>
						@else
						<option value="{!! $cat->id !!}">{{ $cat->name }}</option>
						@endif
						@foreach($cat->subcategory as $catt)
								@if($specialty->category_id == $catt->id)
								<option value="{!! $catt->id !!}" selected>&nbsp;&nbsp;&nbsp;{{ $catt->name }} - {{ $catt->subparent->name }}</option>
								@else
								<option value="{!! $catt->id !!}">&nbsp;&nbsp;&nbsp;{{ $catt->name }}</option>
								@endif
						@endforeach
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="content" class="col-sm-2 control-label">Content</label>
			<div class="col-sm-10">
				<textarea id="content" name="content" placeholder="Masukan konten halaman anda disini..." class="form-control" rows="6">
					{{ $specialty->content }}
				</textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="picture" class="col-sm-2 control-label">Picture</label>
			<div class="col-sm-5">
				<input type="hidden" name="picture_old" value="{{ $specialty->picture }}">
				<div class="upload-file">
					<input type="file" name="picture" id="picture" class="picture">
					<label data-title="Pilih gambar" for="picture">
						<span data-title="Tidak ada gambar dipilih"></span>
					</label>
				</div>
				<br>
			</div>
		</div>
		<div class="form-group">
			<label for="active" class="col-sm-2 control-label">Publish</label>
			<div class="col-lg-10">
				@if ($specialty->active == 'Y')
					<label class="label-radio inline">
						<input type="radio" name="active" value="Y" checked>
						<span class="custom-radio"></span>
						Aktif
					</label>
					<label class="label-radio inline">
						<input type="radio" name="active" value="N">
						<span class="custom-radio"></span>
						Non Aktif
					</label>
		        @else
					<label class="label-radio inline">
						<input type="radio" name="active" value="Y">
						<span class="custom-radio"></span>
						Aktif
					</label>
					<label class="label-radio inline">
						<input type="radio" name="active" value="N" checked>
						<span class="custom-radio"></span>
						Non Aktif
					</label>
		        @endif

			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
				<a onclick="location.href='{{ route('admin.specialty.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</section>
@endsection


@section('style')
<link href="{{ asset('ckeditor/ckeditor/content.css') }}" rel="stylesheet"/>
<link href="{{ asset('asset_admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('ckeditor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('asset_admin/js/jquery.gritter.min.js') }}"></script>
<script>
CKEDITOR.replace( 'content' );
$('.picture').change(function()	{
	var filename = $(this).val().split('\\').pop();
	$(this).parent().find('span').attr('data-title',filename);
	$(this).parent().find('label').attr('data-title','Change file');
	$(this).parent().find('label').addClass('selected');
});
</script>
@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif
@endsection
