<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
		public $timestamps = false;
		// public $incrementing = false;

		public function submenu()
		{
				return $this->hasMany(Menu::class, 'id_parent');
		}

		/*
		 * Relasi ke sesama table menu yaitu id_parent dengan belongsTo
		 */
		public function parent()
		{
				return $this->belongsTo(Menu::class, 'id_parent');
		}
}
