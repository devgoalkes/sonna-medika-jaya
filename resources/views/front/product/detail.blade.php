@extends('layouts.front.template')
@section('title', "Category ". $category->name)
@section('metatitle', "Category ". $category->name)
@section('metadescription', "Category ". $category->name)

@section('content')
<section class="banner-area " id="home">
  <!-- <div class="overlay overlay-bg"></div> -->
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="about-content col-lg-12">
        <!-- <h1 class="text-white">
          {{ $product->name }}
        </h1> -->
        <!-- <p class="text-white link-nav"><a href="{{ route('home') }}">Home </a>
          <span class="lnr lnr-arrow-right"></span>  <a href="{{ route('front.product.category', $category) }}"> {{ $category->name }}</a>
          <span class="lnr lnr-arrow-right"></span>  {{ $product->name }}
        </p> -->

      </div>
    </div>
  </div>
</section>

<section class="products-area product-page pt-30">
	<div class="container">
    <div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">{{ $product->name }}</h2>
			</div>
      <div class="col-md-12 pb-40">
        <div class="pull-left" style="padding-right:20px;">
          <img src="{{ asset('storage/uploads/products/'. $product->picture) }}" alt="{{ $product->name }}" width="200px;">
        </div>
				{!! $product->description !!}
			</div>
		</div>
	</div>
</section>
@endsection

@section('styles')
<style media="screen">
  .blog-row {margin-top:50px; }
  .margin_bottom30 { margin-bottom:30px; }
  .margin_left10 {margin-left:10px; }
  .blog-content {padding:10px; }
  .bg-white {background-color:#fff;}
</style>
@endsection
