<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
   	public $incrementing = false;

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
