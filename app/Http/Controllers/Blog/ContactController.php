<?php

namespace App\Http\Controllers\Blog;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use Ramsey\Uuid\Uuid;
use App\Mail\ContactEmail;
use Illuminate\Http\Request;

use App\Models\{ Setting, Contact };

use Mail;

class ContactController extends Controller
{
    private $name;
    private $email;
    private $subj;
    private $message;

    private $subject;

    public function index(){
      $setting = Setting::first();
      return view('front.contact_us', compact('setting'));
    }

    public function sendMail(ContactRequest $request)
    {
        if($request->contact_subject == 1){
          $this->subject = 'Demo Request';
        }else{
          $this->subject = 'Distributor Over';
        }
        $isi = array("name" =>"PT. Sonna Medika Jaya",
                    "sender_subject" => $this->subject,
                    "sender_message" => $request->contact_message,
                    "sender_email" => $request->contact_email,
                    "sender_name" => $request->contact_name
                  );

        $cs = Setting::first();

        $contact             = new Contact;
        $contact->id         = Uuid::uuid4()->toString();
        $contact->name 	     = $request->contact_name;
        $contact->email 	   = $request->contact_email;
        $contact->subject    = $request->contact_subject;
        $contact->message    = $request->contact_message;
        $contact->read       = '0';
        $contact->created_at = date('Y-m-d H:i:s');

        Mail::to($cs->mail_address)->send(new ContactEmail($contact));
        $contact->save();
        return response()->json('success');
    }
}
