@extends('layouts.admin.template')
@section('title', 'Add Users')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li></i><a onclick="location.href='{{ route('admin.user.index') }}'"> User</a></li>
		 <li class="active">Add</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Add User</div>
	<div class="panel-body">
		{!! Form::open(['route' => 'admin.user.store', 'methode' => 'POST', 'class' => 'form-horizontal']) !!}

		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Username</label>
			<div class="col-sm-10">
				<input type="text" name="name" class="form-control" id="name" placeholder="Username" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">Email</label>
			<div class="col-sm-10">
				<input type="text" name="email" class="form-control" id="email" placeholder="Email" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="passsword" class="col-sm-2 control-label">Password</label>
			<div class="col-sm-10">
				<input type="password" name="password" class="form-control" id="passsword" placeholder="Password" autocomplete="off">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a href="{{ route('admin.user.index') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection
