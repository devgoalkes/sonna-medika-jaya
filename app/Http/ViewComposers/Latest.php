<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Article;

class Latest
{

  	public $latest;


  	function __construct(Article $latest)
  	{
  		$this->latest = Article::limit(5)->get();
  	}

  	public function compose(View $view)
  	{
  		$view->with('latest', $this->latest);
  	}
}
