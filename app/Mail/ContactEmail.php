<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Contact;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $contact;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->contact->subject == 1){
          $subject = 'Demo Request';
        }
        if($this->contact->subject == 2){
          $subject = 'Distributor Over';
        }
        return $this->subject($subject)->markdown('emails.contact')
                ->with([
                    'contact' => $this->contact,
                    'subject' => $subject
                ])->from('info@sonnamedika.co.id','Sonna Medika Jaya');
    }
}
