@extends('layouts.front.template')
@section('title', "501 Not Implemented")
@section('metatitle', "501 Not Implemented")
@section('metadescription', "501 Not Implemented")
@section('content')
<section class="blog-posts-area pt-120 mt-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 post-list blog-post-list text-center">
				<div class="single-post">
					<a href="#">
						<h2>501!</h2>
					</a>
          <hr>
					<div class="content-wrap">

            <h5>501 Not Implemented</h5>
            <p>Oh no, server error for response the data!</p>
            <a href="{{ url('/') }}" class="default-btn">Back to Home</a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
