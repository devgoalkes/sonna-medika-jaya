@extends('layouts.front.template')
@section('title', "401 Unauthorized Access")
@section('metatitle', "401 Unauthorized Access")
@section('metadescription', "401 Unauthorized Access")
@section('content')
<section class="blog-posts-area pt-120 mt-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 post-list blog-post-list">
				<div class="single-post">
					<a href="#">
						<h2>401!</h2>
					</a>
          <hr>
					<div class="content-wrap">

            <h5>401 Unauthorized</h5>
            <p>You're unauthorized to access this page.</p>
            <a href="{{ url('/') }}" class="default-btn">Back to Home</a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
