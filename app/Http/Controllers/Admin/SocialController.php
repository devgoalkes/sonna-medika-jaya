<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;

use App\Models\{ Social, Setting };
use Ramsey\Uuid\Uuid;
use DataTables;
use Session;
use Alert;
class SocialController extends Controller
{
    public function index(Builder $builder)
    {
      if (request()->ajax()) {
          return DataTables::of(Social::all())
              ->addColumn('link', function ($social) {
                  return '<div class="btn-group"><button type="button" onclick="konfirm(this)" data-href="'. route('admin.social.destroy', $social).'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                  </div>';
              })
              ->addColumn('created_at', function ($social) {
                  return tglWaktuIndo($social->created_at);
              })
              ->rawColumns(['link'])
              ->addIndexColumn()
              ->make(true);
      }

      $html = $builder->columns([
          ['data' => 'DT_Row_Index', 'name' => 'id', 'title' => 'No', 'searchable'=> false, 'orderable' => false],
          ['data' => 'driver', 'name' => 'driver', 'title' => 'Social Media', 'searchable'=> true],
          ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Date', 'searchable'=> false],
          ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
      ])->parameters([
          'stateSave' => 'true',
          'stateSaveCallback',
          'sPaginationType' => 'full_numbers'
      ]);

      return view('admin.social.index', compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.social.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $setting = Setting::first();
        $social = new Social;

        $social->driver     = $request->driver;
        $social->setting_id = $setting->id;
        $social->username   = $request->username;
        $social->created_at = date('Y-m-d H:i:s');
        $social->save();

        Alert::success('Data berhasil disimpan', 'Sukses');
        return redirect()->route('admin.social.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Social $social)
    {
        return view('admin.social.edit', $social);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Social $social)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Social $social)
    {
        $social->delete();
        Alert::success('Data berhasil dihapus', 'Sukses');
        return redirect()->route('admin.social.index');
    }
}
