@extends('layouts.admin.template')
@section('title', 'Tambah Specialty')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li></i><a onclick="location.href='{{ route('admin.specialty.index') }}'">Specialty</a></li>
		 <li class="active">Tambah</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">@yield('title')</div>
	<div class="panel-body">
		{!! Form::open(array('route' => 'admin.specialty.store', 'methode' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) !!}

		<div class="form-group">
			<label for="title" class="col-sm-2 control-label">Judul Halaman</label>
			<div class="col-sm-10">
				<input type="text" name="title" class="form-control" id="title" placeholder="Judul Halaman" value="">
			</div>
		</div>
		<div class="form-group">
			<label for="category_id" class="col-sm-2 control-label">Category</label>
			<div class="col-sm-3">
				<select class="form-control" name="category_id">
					@if(!$category->subcategory->isEmpty())
						<option value="">No Category</option>
						@foreach($category->subcategory as $cat)
							<option value="{!! $cat->id !!}">{{ $cat->name }}</option>
							@foreach($cat->subcategory as $catt)
									<option value="{!! $catt->id !!}">&nbsp;&nbsp;&nbsp;{{ $catt->name }}</option>
							@endforeach
						@endforeach
					@else
						<option value="">No Category</option>
					@endif
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="content" class="col-sm-2 control-label">Konten Halaman</label>
			<div class="col-sm-10">
				<textarea id="content" name="content" placeholder="Masukan konten halaman disini..." class="form-control" rows="6"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="picture" class="col-sm-2 control-label">Gambar</label>
			<div class="col-sm-5">
				<div class="upload-file">
					<input type="file" name="picture" id="picture" class="picture">
					<label data-title="Pilih gambar" for="picture">
						<span data-title="Tidak ada gambar dipilih"></span>
					</label>
				</div>
				<br>
			</div>
		</div>
		<div class="form-group">
			<label for="active" class="col-sm-2 control-label">Publish</label>
			<div class="col-lg-10">
				<label class="label-radio inline">
					<input type="radio" name="active" value="Y">
					<span class="custom-radio"></span>
					Active
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('admin.article.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('ckeditor/content.css') }}" rel="stylesheet"/>
<link href="{{ asset('asset_admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('ckeditor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('asset_admin/js/jquery.gritter.min.js') }}"></script>
<script>
	CKEDITOR.replace( 'content');

	$('.picture').change(function()	{
		var filename = $(this).val().split('\\').pop();
		$(this).parent().find('span').attr('data-title',filename);
		$(this).parent().find('label').attr('data-title','Change file');
		$(this).parent().find('label').addClass('selected');
	});
</script>

@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif

@endsection
