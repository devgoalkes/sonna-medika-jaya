@extends('layouts.front.template')
@section('title', '')
@section('metatitle', '')
@section('metadescription', '')

@section('content')

	<section class="blog-posts-area section-gap">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 post-list blog-post-list">
					@foreach($articles as $article)
					<div class="single-post">
						<img class="img-fluid" src="{{ asset('storage/uploads/articles/'. $article->picture ) }}" alt="">
						<a href="{{ route('front.article.detail', $article) }}">
							<h1 class="h3">{{ $article->title }}</h1>
						</a>
						<p>
							{!! str_limit($article->content, 100) !!}
						</p>
					</div>
					<hr>
					@endforeach
					{{ $articles->links() }}
				</div>
				@include('layouts.front.partials._sidebar')
			</div>
		</div>
	</section>
@endsection
