@extends('layouts.admin.template')
@section('title', 'Halaman')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li class="active">@yield('title')</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">&nbsp;</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{ route('admin.page.create') }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Tambah @yield('title') </a>
		@endsection
		{!! $html->table(['class' => 'table table-striped']) !!}
	</div>
</div>
<div class="modal fade modal-detail" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg" style="overflow-y: scroll; max-height:85%; margin-top: 50px; margin-bottom:50px;" >
    <div class="modal-content">
      <div class="modal-body">
        <div id="modalcontent"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('asset_admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('asset_admin/js/clipboard.js') }}"></script>
<script src="{{ asset('asset_admin/js/sweetalert.min.js') }}"></script>
<script type="text/javascript">
	 function show(target){
		 var target = target.getAttribute('data-href');
		 $('#modalcontent').load(target);
	 }

	 function konfirm(href){
			 var href = href.getAttribute('data-href');
			 swal({
					 title: "Are you sure?",
					 text: "You will not be able to recover this data file!",
					 type: "warning",
					 showCancelButton: true,
					 confirmButtonColor: "#DD6B55",
					 confirmButtonText: "Yes, delete it!",
					 closeOnConfirm: false,
					 showLoaderOnConfirm: true
			 },
			 function(){
					 $.ajax({
							 type: "POST",
							 url: href,
							 data: {
									 '_token': "{{ csrf_token() }}",
									 '_method' : 'DELETE'
							 },
							 success: function() {
									 $('#dataTableBuilder').DataTable().ajax.reload();
									 swal("Deleted!", "Help Berhasil Dihapus!", "success");
							 },
							 error: function() {
									 swal("Error!", "Help Gagal Dihapus!", "error");
							 }
					 });
			 });
	 }
</script>
{!! $html->scripts() !!}
@endsection
