@extends('layouts.admin.template')
@section('title', 'Edit Product')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li></i><a onclick="location.href='{{ route('admin.product.index') }}'"> Product</a></li>
		 <li class="active">Add</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">Edit Product</div>
	<div class="panel-body">
		{!! Form::open(array('route' => ['admin.product.update', $product->id], 'method' => 'PUT', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) !!}
		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Product Name</label>
			<div class="col-sm-10">
				<input type="text" name="name" class="form-control" id="name" autocomplete="off" value="{{ $product->name }}">
			</div>
		</div>

		<div class="form-group">
			<label for="category_id" class="col-sm-2 control-label">Category</label>
			<div class="col-sm-4">
				<select class="form-control" name="category_id">
					<option value="">No Category</option>
					@foreach($categories->subcategory as $category)
						@if($product->category_id == $category->id)
						<option value="{!! $category->id !!}" selected>{{ $category->name }}</option>
						@else
						<option value="{!! $category->id !!}">{{ $category->name }}</option>
						@endif
						@foreach($category->subcategory as $cat)
								@if($product->category_id == $cat->id)
								<option value="{!! $cat->id !!}" selected>&nbsp;&nbsp;&nbsp;{{ $cat->name }}</option>
								@else
								<option value="{!! $cat->id !!}">&nbsp;&nbsp;&nbsp;{{ $cat->name }}</option>
								@endif
						@endforeach
					@endforeach
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Description</label>
			<div class="col-sm-10">
				<textarea class="form-control" id="description" name="description" rows="4" style="width:100%">{{ $product->description }}</textarea>
			</div>
		</div>

		<div class="form-group">
			<label for="picture" class="col-sm-2 control-label">Picture</label>
			<div class="col-sm-5">
				<input type="text" name="picture_old" class="form-control" value="{{ $product->picture }}">
				<div class="upload-file">
					<input type="file" name="picture" id="picture" class="picture">
					<label data-title="Select file" for="picture">
						<span data-title="No file selected..."></span>
					</label>
				</div>
				<br>
			</div>
		</div>

		<div class="form-group">
			<label for="active" class="col-sm-2 control-label">Active</label>
			<div class="col-lg-10">
        @if($product->active == 'Y')
				<label class="label-radio inline">
					<input type="radio" name="active" checked value="Y">
					<span class="custom-radio"></span>
					Active
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
        @else
        <label class="label-radio inline">
					<input type="radio" name="active" value="Y">
					<span class="custom-radio"></span>
					Active
				</label>
				<label class="label-radio inline">
					<input type="radio" name="active" checked value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
        @endif
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('admin.product.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('ckeditor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('asset_admin/js/jquery.gritter.min.js') }}"></script>
<script>
	CKEDITOR.replace( 'description');
	$('.picture').change(function()	{
		var filename = $(this).val().split('\\').pop();
		$(this).parent().find('span').attr('data-title',filename);
		$(this).parent().find('label').attr('data-title','Change file');
		$(this).parent().find('label').addClass('selected');
	});
</script>

@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif

@endsection
