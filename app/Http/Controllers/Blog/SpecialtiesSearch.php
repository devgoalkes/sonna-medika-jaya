<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;

class SpecialtiesSearch extends Controller
{
    public function __invoke(Request $request)
    {
        $specialties = Page::where('title', 'LIKE', "%$request->q%")->paginate('10');
        // dd($specialities);
        return view('front.search', compact('specialties'));
    }
}
