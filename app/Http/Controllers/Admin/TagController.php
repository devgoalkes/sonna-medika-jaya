<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Requests\TagRequest;
use Illuminate\Support\Str;

use App\Models\Tag;
use Ramsey\Uuid\Uuid;
use Session;
use Alert;

class TagController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Tag::get())
                ->addColumn('link', function ($tag) {
                    return '
                    <div class="btn-group">
                    <button type="button" onclick="show(this)" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".modal-detail" data-href="'. route('admin.tag.show', $tag) .'"><i class="fa fa-eye"></i></button>
                    <a href="'. route('admin.tag.edit', $tag) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                    <button type="button" onclick="konfirm(this)" data-href="'. route('admin.tag.destroy', $tag) .'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </div>
                    ';
                })
                ->addColumn('created_at', function ($tag) {
                    return tglWaktuIndo($tag->created_at);
                })
                ->rawColumns(['link', 'active'])
                ->addIndexColumn()
                ->make(true);
        }

        $html = $builder->columns([
            ['data' => 'DT_Row_Index', 'name' => 'id', 'title' => 'No', 'searchable'=> false, 'orderable' => false],
            ['data' => 'name', 'name' => 'name', 'title' => 'Nama', 'searchable'=> true],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tanggal', 'searchable'=> true],
            ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
        ])->parameters([
            'stateSave' => 'true',
            'stateSaveCallback',
            'sPaginationType' => 'full_numbers'
        ]);
        return view('admin.tag.index', compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
      $data['id'] = Uuid::uuid4()->toString();
      $data['name'] = $request->name;
      $data['slug'] = Str::slug($request->name);
      $data['created_at'] = date('Y-m-d H:i:s');

      if (Tag::insert($data)) {
          Alert::success('Data berhasil ditambah !', 'Sukses');
          return redirect()->route('admin.tag.index');
       }else{
          Alert::error('Silahkan di coba lagi !', 'Error');
          return redirect()->route('admin.tag.index');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id!='') {
            $data['tags'] = Tag::where(array('id' => $id))->get()->first();
            return view('admin.tag.edit', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id != '') {
            if (Tag::where('id', $id)->delete()){
                Alert::success('Data berhasil dihapus', 'Sukses');
                return redirect()->route('admin.tag.index');
            }else{
                Alert::error('Data tidak berhasil dihapus', 'Error');
                return redirect()->route('admin.tag.index');
            }
        }
    }
}
