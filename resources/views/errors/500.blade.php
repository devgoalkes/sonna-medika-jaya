@extends('layouts.front.template')
@section('title', "500 Internal Server Error")
@section('metatitle', "500 Internal Server Error")
@section('metadescription', "500 Internal Server Error")
@section('content')
<section class="blog-posts-area pt-120 mt-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 post-list blog-post-list text-center">
				<div class="single-post">
					<a href="#">
						<h2>500!</h2>
					</a>
          <hr>
					<div class="content-wrap">

            <h5>500 Internal Server Error</h5>
            <p>Something went wrong, But we are working on it!</p>
            <a href="{{ url('/') }}" class="default-btn">Back to Home</a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
