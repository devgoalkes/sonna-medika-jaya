<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;

use App\Models\{ Specialty, Category };
use Ramsey\Uuid\Uuid;
use DataTables, Alert, Storage;

class SpecialtyController extends Controller
{

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Specialty::get())
                ->addColumn('link', function ($specialty) {
                    return '
                    <div class="btn-group">
                    <button type="button" onclick="show(this)" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".modal-detail" data-href="'. route('admin.specialty.show', $specialty) .'"><i class="fa fa-eye"></i></button>
                    <a href="'. route('admin.specialty.edit', $specialty) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                    <button type="button" onclick="konfirm(this)" data-href="'. route('admin.specialty.destroy', $specialty) .'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </div>
                    ';
                })
                ->addColumn('created_at', function ($specialty) {
                    return tglWaktuIndo($specialty->created_at);
                })
                ->rawColumns(['link'])
                ->addIndexColumn()
                ->make(true);
        }

        $html = $builder->columns([
            ['data' => 'DT_Row_Index', 'name' => 'id', 'title' => 'No', 'searchable'=> false, 'orderable' => false],
            ['data' => 'title', 'name' => 'title', 'title' => 'Judul', 'searchable'=> true],
            ['data' => 'active', 'name' => 'active', 'title' => 'Aktif', 'searchable'=> false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tanggal', 'searchable'=> true],
            ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
        ])->parameters([
            'stateSave' => 'true',
            'stateSaveCallback',
            'sPaginationType' => 'full_numbers'
        ]);
        return view('admin.specialty.index', compact('html'));
    }

    public function create()
    {
        $category = Category::where('active', 'Y')->where('slug', 'specialty')->first();
        return view('admin.specialty.create', compact('category'));
    }

    public function store(Request $request)
    {
        $specialty = new Specialty;

        if ($request->hasFile('picture')) {
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/specialty'), $photoName);
            $specialty->picture    = $photoName;
        }
        $specialty->id          = Uuid::uuid4()->toString();
        $specialty->title       = $request->title;
        $specialty->category_id = $request->category_id;
        $specialty->slug        = str_slug($request->title);
        $specialty->content     = $request->content;
        $specialty->active      = $request->active;
        $specialty->created_at  = date('Y-m-d H:i:s');

        if ($specialty->save()) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.specialty.index');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.create.index');
         }
    }

    public function show(Specialty $specialty)
    {
        return view('admin.specialty.show', compact('specialty'));
    }

    public function edit(Specialty $specialty)
    {
        $category = Category::where('active', 'Y')->where('slug', 'specialty')->first();
        return view('admin.specialty.edit', compact('specialty', 'category'));
    }

    public function update(Request $request, Specialty $specialty)
    {
        if ($request->hasFile('picture')) {
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/specialty'), $photoName);

            $specialty->picture      = $photoName;
        }else{
            $specialty->picture      = $request->picture_old;
        }
        $specialty->title        = $request->title;
        $specialty->category_id  = $request->category_id;
        $specialty->slug         = str_slug($request->title);
        $specialty->content      = $request->content;
        $specialty->active       = $request->active;
        $specialty->updated_at   = date('Y-m-d H:i:s');

        $specialty->save();
        Alert::success('Data berhasil disimpan', 'Sukses');
        return redirect()->route('admin.specialty.index');
    }

    public function destroy(Specialty $specialty)
    {
        if ($specialty->delete()){
            Storage::delete('uploads/specialty/'.$specialty->picture);
        }
    }
}
