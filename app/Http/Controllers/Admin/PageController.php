<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;
use Yajra\DataTables\Html\Builder;

use App\Models\{ Page };
use Ramsey\Uuid\Uuid;
use DataTables, Alert, Storage;

class PageController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Page::get())
                ->addColumn('link', function ($page) {
                    return '
                    <div class="btn-group">
                    <button type="button" onclick="show(this)" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".modal-detail" data-href="'. route('admin.page.show', $page) .'"><i class="fa fa-eye"></i></button>
                    <a href="'. route('admin.page.edit', $page) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                    <button type="button" onclick="konfirm(this)" data-href="'. route('admin.page.destroy', $page) .'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </div>
                    ';
                })
                ->addColumn('created_at', function ($page) {
                    return tglWaktuIndo($page->created_at);
                })
                ->rawColumns(['link'])
                ->addIndexColumn()
                ->make(true);
        }

        $html = $builder->columns([
            ['data' => 'DT_Row_Index', 'name' => 'id', 'title' => 'No', 'searchable'=> false, 'orderable' => false],
            ['data' => 'title', 'name' => 'title', 'title' => 'Judul', 'searchable'=> true],
            ['data' => 'active', 'name' => 'active', 'title' => 'Aktif', 'searchable'=> false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tanggal', 'searchable'=> true],
            ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
        ])->parameters([
            'stateSave' => 'true',
            'stateSaveCallback',
            'sPaginationType' => 'full_numbers'
        ]);
        return view('admin.page.index', compact('html'));
    }

    public function create()
    {
        return view('admin.page.create');
    }

    public function store(PageRequest $request)
    {
        if ($request->hasFile('picture')) {

          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/pages'), $photoName);
          $data['id']         = Uuid::uuid4()->toString();
          $data['title']      = $request->title;
          $data['slug']       = str_slug($request->title);
          $data['content']    = $request->content;
          $data['picture']    = $photoName;
          $data['active']     = $request->active;
          $data['created_at'] = date('Y-m-d H:i:s');
        } else {

          $data['id']         = Uuid::uuid4()->toString();
          $data['title']      = $request->title;
          $data['slug']       = str_slug($request->title);
          $data['content']    = $request->content;
          $data['active']     = $request->active;
          $data['created_at'] = date('Y-m-d H:i:s');
        }

        if (Page::insert($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.page.index');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.create.index');
         }
    }

    public function show(Page $page)
    {
        return view('admin.page.show', compact('page'));
    }

    public function edit(Page $page)
    {
        return view('admin.page.edit', compact('page'));
    }

    public function update(PageRequest $request, Page $page)
    {
        if (isset($request->picture)) {
          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/pages'), $photoName);

          $page->picture      = $photoName;
        }else{
          $page->picture      = $request->picture_old;
        }
        $page->title        = $request->title;
        $page->slug         = str_slug($request->title);
        $page->content      = $request->content;
        $page->active       = $request->active;
        $page->updated_at   = date('Y-m-d H:i:s');

        if ($page->save()) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.page.index');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.page.edit', $page);
         }
    }

    public function destroy(Page $page)
    {
        if ($page->delete()){
            Storage::delete('upload/pages/'.$page->picture);
        }
    }
}
