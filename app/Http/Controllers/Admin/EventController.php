<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\EventRequest;
use Yajra\DataTables\Html\Builder;

use App\Models\{ Event, Category };
use Ramsey\Uuid\Uuid;
use DataTables, Alert, Storage;

class EventController extends Controller
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return DataTables::of(Event::get())
                ->addColumn('link', function ($event) {
                    return '
                    <div class="btn-group">
                    <button type="button" onclick="show(this)" class="btn btn-info btn-xs btn-detail" data-toggle="modal" data-target=".modal-detail" data-href="'. route('admin.event.show', $event) .'"><i class="fa fa-eye"></i></button>
                    <a href="'. route('admin.event.edit', $event) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                    <button type="button" onclick="konfirm(this)" data-href="'. route('admin.event.destroy', $event) .'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </div>
                    ';
                })
                ->addColumn('created_at', function ($event) {
                    return tglWaktuIndo($event->created_at);
                })
                ->rawColumns(['link'])
                ->addIndexColumn()
                ->make(true);
        }

        $html = $builder->columns([
            ['data' => 'DT_Row_Index', 'name' => 'id', 'title' => 'No', 'searchable'=> false, 'orderable' => false],
            ['data' => 'title', 'name' => 'title', 'title' => 'Judul', 'searchable'=> true],
            ['data' => 'active', 'name' => 'active', 'title' => 'Aktif', 'searchable'=> false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Tanggal', 'searchable'=> true],
            ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
        ])->parameters([
            'stateSave' => 'true',
            'stateSaveCallback',
            'sPaginationType' => 'full_numbers'
        ]);
        return view('admin.event.index', compact('html'));
    }

    public function create()
    {
        return view('admin.event.create');
    }

    public function store(EventRequest $request)
    {
        $event = new Event;
        $category = Category::where('slug', 'events')->first();

        if ($request->hasFile('picture')) {
          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/events'), $photoName);
          $event->picture = $photoName;
        }

        $event->id         = Uuid::uuid4()->toString();
        $event->title      = $request->title;
        $event->slug       = str_slug($request->title);
        $event->category_id= $category->id;
        $event->content    = $request->content;
        $event->active     = $request->active;
        $event->created_at = date('Y-m-d H:i:s');

        if ($event->save()) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.event.index');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.event.create');
         }
    }

    public function show(Event $event)
    {
        return view('admin.event.show', compact('event'));
    }

    public function edit(Event $event)
    {
        return view('admin.event.edit', compact('event'));
    }

    public function update(Request $request, Event $event)
    {
        if ($request->hasFile('picture')) {
            $photoName = time().'.'.$request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('storage/uploads/events'), $photoName);
            $event->picture   = $photoName;
        } else {
            $event->picture      = $request->picture_old;
        }
        $event->title        = $request->title;
        $event->slug         = str_slug($request->title);
        $event->content      = $request->content;
        $event->active       = $request->active;
        $event->updated_at   = date('Y-m-d H:i:s');

        if ($event->save()) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.event.index');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.event.edit', $event);
         }
    }

    public function destroy(Event $event)
    {
        if ($event->delete()){
            Storage::delete('uploads/events/'.$event->picture);
        }
    }
}
