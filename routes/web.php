<?php
Route::group(['namespace' => 'Blog'], function() {
    Route::name('home')->get('/', 'HomeController@index');
    Route::name('front.page')->get('/pages/{page}', 'ShowPage');
    Route::name('front.specialties.search')->get('/specialties/search', 'SpecialtiesSearch');

    Route::name('front.article.index')->get('/article', 'ArticleController@index');
    Route::name('front.article.category')->get('/article/category/{category}', 'ShowArticleCategory');
    Route::name('front.article.detail')->get('/article/{article}', 'ArticleController@detail');

    Route::name('front.event.index')->get('/events', 'EventController@index');
    Route::name('front.event.detail')->get('/events/{event}', 'EventController@detail');
    Route::name('front.event.search')->get('/events/search', 'EventController@search');

    Route::name('front.product.index')->get('/product', 'ProductController@index');
    Route::name('front.product.category')->get('/product/category/{category}', 'ProductController@category');
    Route::name('front.product.detail')->get('/product/{category}/{product}', 'ProductController@detail');
    Route::name('front.product.search')->get('/product/search', 'ProductController@search');

    Route::name('front.specialty.index')->get('/specialty', 'SpecialtyController@index');
    Route::name('front.specialty.category')->get('/specialty/{category}', 'SpecialtyController@category');
    Route::name('front.specialty.detail')->get('/specialty/{category}/{specialty}', 'SpecialtyController@detail');

    Route::name('front.blog.index')->get('/blog', 'ArticleController@index');

    Route::name('front.contact-us')->get('contact-us', 'ContactController@index');
    Route::name('front.partnership')->get('partnership', 'PartnershipController@index');
    Route::name('front.contact')->post('/home/contact', 'ContactController@sendMail');
});

Route::group(['prefix' => 'admin'], function () {
  Auth::routes();

  Route::group(['middleware' => 'auth','namespace' => 'Admin'], function() {
    Route::name('admin')->get('/', 'HomeController@index');
    Route::resource('/article', 'ArticleController', ['names' => 'admin.article']);

    Route::name('admin.category.index')->get('/category', 'CategoryController@index');
    Route::name('admin.category.create')->get('/category/create', 'CategoryController@create');
    Route::name('admin.category.show')->get('/category/{id}/show', 'CategoryController@show');
    Route::name('admin.category.edit')->get('/category/edit/{id}', 'CategoryController@edit');
    Route::name('admin.category.destroy')->delete('/category/{id}', 'CategoryController@destroy');
    Route::name('admin.category.store')->post('/category', 'CategoryController@store');
    Route::name('admin.category.update')->put('/category/{id}', 'CategoryController@update');

    Route::resource('/contact', 'ContactController', ['names' => 'admin.contact']);
    Route::resource('/event', 'EventController', ['names' => 'admin.event']);
    Route::resource('/menu', 'MenuController', ['names' => 'admin.menu']);
    Route::resource('/page', 'PageController', ['names' => 'admin.page']);
    Route::name('admin.product.index')->get('/product', 'ProductController@index');
    Route::name('admin.product.create')->get('/product/create', 'ProductController@create');
    Route::name('admin.product.show')->get('/product/{id}/show', 'ProductController@show');
    Route::name('admin.product.edit')->get('/product/edit/{id}', 'ProductController@edit');
    Route::name('admin.product.destroy')->delete('/product/{id}', 'ProductController@destroy');
    Route::name('admin.product.store')->post('/product', 'ProductController@store');
    Route::name('admin.product.update')->put('/product/{id}', 'ProductController@update');

    Route::resource('/setting', 'SettingController', ['names' => 'admin.setting']);
    Route::resource('/social', 'SocialController', ['names' => 'admin.social']);
    Route::resource('/specialty', 'SpecialtyController', ['names' => 'admin.specialty']);
    Route::resource('/tag', 'TagController', ['names' => 'admin.tag']);
    Route::resource('/user', 'UserController', ['names' => 'admin.user']);

    Route::name('admin.password.edit')->get('/profil/password/{user}/edit', 'UserProfileController@editPassword');
    Route::name('admin.password.update')->put('/profil/password/{user}', 'UserProfileController@updatePassword');
    Route::name('admin.profil.index')->get('/profil', 'UserProfileController@index');
    Route::name('admin.profil.create')->get('/profil/create', 'UserProfileController@create');
    Route::name('admin.profil.store')->post('/profil/store', 'UserProfileController@store');
    Route::name('admin.profil.update')->put('/profil/update/{user}', 'UserProfileController@update');
    Route::name('admin.profil.edit')->get('/profil/edit/{user}', 'UserProfileController@edit');

    Route::name('admin.user.edit')->get('/user/edit/{user}', 'UserController@edit');
    Route::name('admin.user.update')->put('/user/update/{user}', 'UserController@update');
  });

});
