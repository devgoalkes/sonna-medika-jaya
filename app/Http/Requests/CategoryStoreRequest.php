<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama Kategori wajib diisi !',
            'name.min' => 'Minimal nama kategory terdiri dari 5 karakter !',
            'status.required' => 'Status wajib diisi !',
        ];
    }
}
