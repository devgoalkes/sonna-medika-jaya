<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Setting;

class Setting
{

  	public $setting;


  	function __construct(Setting $setting)
  	{
  		$this->setting = Setting::first();
  	}

  	public function compose(View $view)
  	{
  		$view->with('setting', $this->setting);
  	}
}
