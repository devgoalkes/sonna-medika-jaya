<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserProfileRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\{ User, UserProfile };
use Ramsey\Uuid\Uuid;
use Session, Hash, Alert, Auth;

class UserProfileController extends Controller
{

  public function index(Request $request)
  {
      $user = User::find(Auth::user()->id);

      return view('admin.user_profil.index', compact('user'));
  }

  public function create()
  {
      $user = Auth::user();
      return view('admin.user_profil.create', compact('user'));
  }

  public function store(UserProfileRequest $request)
  {
      $data['id']           = Uuid::uuid4()->toString();
      $data['user_id']      = Auth::user()->id;
      $data['full_name']    = $request->full_name;
      $data['sex']          = $request->sex;
      $data['address']      = $request->address;
      $data['phone_number'] = $request->phone_number;
      $data['facebook']     = $request->facebook;
      $data['twitter']      = $request->twitter;
      $data['google']       = $request->google;
      $data['youtube']      = $request->youtube;
      $data['path']         = $request->path;
      $data['description']  = $request->description;
      $data['created_at']   = date('Y-m-d H:i:s');

      if (UserProfile::insert($data)) {
          Alert::success('Data berhasil disimpan', 'Sukses');
          return redirect()->route('admin.profil.index');
       }else{
          Alert::error('Silahkan coba lagi !', 'Error');
          return redirect()->route('admin.profil.index');
       }
  }

  public function edit(User $user)
  {
      return view('admin.user_profil.edit', compact('user'));
  }

  public function update(UserProfileRequest $request, User $user)
  {
      $user->user_profile->user_id      = $request->user_id;
      $user->user_profile->full_name    = $request->full_name;
      $user->user_profile->sex          = $request->sex;
      $user->user_profile->address      = $request->address;
      $user->user_profile->phone_number = $request->phone_number;
      $user->user_profile->facebook     = $request->facebook;
      $user->user_profile->twitter      = $request->twitter;
      $user->user_profile->google       = $request->google;
      $user->user_profile->youtube      = $request->youtube;
      $user->user_profile->path         = $request->path;
      $user->user_profile->description  = $request->description;
      $user->user_profile->updated_at   = date('Y-m-d H:i:s');

      if ($user->user_profile->save()) {
          Alert::success('Data berhasil disimpan', 'Sukses');
          return redirect()->route('admin.profil.index');
       }else{
          Alert::error('Silahkan di coba lagi !', 'Error');
          return redirect()->route('admin.profil.edit', $user);
       }
  }

  public function editPassword(User $user)
  {
        return view('admin.user_profil.edit_password', compact('user'));
  }

  public function updatePassword(ChangePasswordRequest $request, User $user)
  {
      if(Hash::check($request->old_password, Auth::user()->password)) {
          $user->password = bcrypt($request->password);
          $user->update();
          Alert::success('Password berhasil diubah', 'Sukses');
          return redirect()->route('admin.profil.index');
      }else{
        Alert::error('Gagal update password.', 'Error');
        return redirect()->route('admin.password.update', $user);
      }


  }
}
