<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Page;

class ShowPage extends Controller
{
    public function __invoke(Page $page, Request $request)
    {
        return view('front.page', compact('page'));
    }
}
