@extends('layouts.front.template')
@section('metatitle', "Search Our Specialties")
@section('metadescription', "Our Specialties")
@section('title', 'Our Specialties')

@section('content')

<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
            </div>
        </div>
    </div>
</section>

<section class="products-area product-page pt-40">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">Our Specialties</h2>
        @if($specialties->count() > 0)
				<p>
				Found <b>{{ $specialties->count() }}</b> events with the keywords "{{ request()->q }}".
				</p>
        @endif
			</div>
		</div>
		<div class="row justify-content-md-center">
      @forelse($specialties as $specialty)
			<div class="col-lg-3 col-md-6">
				<div class="single-product">
					<div class="details">
						<h4>{!! str_limit(strip_tags($specialty->title), 20) !!}</h4>
						<p>
							{!! str_limit(strip_tags($specialty->content), 100) !!}
						</p>
						<a href="{{ route('front.page', $specialty) }}" class="primary-btn text-uppercase">View Details</a>
					</div>
				</div>
			</div>
			@empty
      <div class="col-md-12 pb-40 text-center">
        <h4>Sorry, the specialties with the keyword "{{ request()->q }}" was not found.</h4>
      </div>

      <div class="col-md-6 pb-40 justify-content-md-center">
        <form method="get" class="form-horizontal" action="{{ route('front.specialties.search') }}" role="search">
          {{ csrf_field() }}
          <div class="input-group-icon mt-10">
            <input type="text" name="q" placeholder="Search our specialties" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search our specialties'" required class="single-input-primary search-input">
            <button type="submit" class="icon"><i class="fa fa-search fa-lg" aria-hidden="true"></i></button>
          </div>
        </form>
      </div>
      @endforelse
		</div>
	</div>
</section>

@endsection
