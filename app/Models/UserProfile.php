<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
  public $incrementing = false;

  public function user()
  {
    return $this->belongsTo(UserProfile::class);
  }
}
