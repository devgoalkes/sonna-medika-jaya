@extends('layouts.front.template')
@section('title', $article->title)
@section('metatitle', $article->title)
@section('metadescription', $article->title)

@section('content')
<section class="banner-area " id="home">
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="about-content col-lg-12">

      </div>
    </div>
  </div>
</section>

<section class="products-area product-page pt-30">
	<div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-md-8 pb-40">
        <img class="img-fluid" src="{{ asset('storage/uploads/articles/'. $article->picture ) }}" alt="">
        <h1 class="pb-20 pt-20">{{ $article->title }}</h1>
				{!! $article->content !!}
			</div>
      @include('layouts.front.partials._sidebar')
		</div>

	</div>
</section>
@endsection
