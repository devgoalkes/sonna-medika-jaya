<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	  'id' => Uuid::uuid4()->toString(),
            'name' => 'admin',
            'email' => 'admin@sonnamedika.com',
            'password' => bcrypt('qwerty'),
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
