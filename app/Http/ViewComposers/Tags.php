<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Tag;

class Tags
{

  	public $categories;


  	function __construct(Tag $tags)
  	{
  		$this->tags = Tag::all();
  	}

  	public function compose(View $view)
  	{
  		$view->with('tags', $this->tags);
  	}
}
