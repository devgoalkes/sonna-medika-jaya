<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{ Faq, Category };

class FaqController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('front.faqs.index', compact('categories'));
    }

    public function category(Category $category)
		{
			$faqs = $category->faqs()->orderBy('created_at', 'desc')->paginate(10);
			return view('front.faqs.category', compact(['faqs', 'category']));
		}
}
