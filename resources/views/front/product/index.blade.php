@extends('layouts.front.template')
@section('metatitle', "Our Products")
@section('metadescription', "Our Products")
@section('title', 'Our Products')

@section('content')
<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
            </div>
        </div>
    </div>
</section>

<section class="products-area product-page pt-40">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">Our Products</h2>
				<p>
					Who are in extremely love with eco friendly system.
				</p>
			</div>
		</div>
		<div class="row">
      @foreach($products as $product)
			<div class="col-lg-3 col-md-6">
				<div class="single-product">
					<div class="thumb">
						<img src="{{ asset('storage/uploads/products/'. $product->picture) }}" alt="{{ $product->name }}" width="100%" height="150px">
					</div>
					<div class="details">
            <h3>{!! str_limit(strip_tags($product->brand), 20) !!}</h3><br>
						<h5>{!! str_limit(strip_tags($product->name), 20) !!}</h5>
						<p>
							{!! str_limit(strip_tags($product->description), 100) !!}
						</p>
						<a href="{{ route('front.product.detail', [$product->category,$product]) }}" class="primary-btn text-uppercase">View Details</a>
					</div>
				</div>
			</div>
      @endforeach
		</div>
	</div>
</section>
@endsection
