@extends('layouts.front.template')
@section('title', $event->name)
@section('metatitle', $event->name)
@section('metadescription', $event->name)

@section('content')

<section class="products-area product-page pt-120">
	<div class="container">
    <div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">{{ $event->title }}</h2>
			</div>
      <div class="col-md-12 pb-40">
        <div class="single-post">
          <div class="pull-left" style="padding-right:20px;">
            <img class="img-fluid" src="{{ asset('storage/uploads/events/'. $event->picture) }}" alt="{{ $event->name }}" width="300px">
          </div>
          <div class="content-wrap">
    				{!! $event->content !!}
          </div>
        </div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('styles')
<style media="screen">
  .blog-row {margin-top:50px; }
  .margin_bottom30 { margin-bottom:30px; }
  .margin_left10 {margin-left:10px; }
  .blog-content {padding:10px; }
  .bg-white {background-color:#fff;}
</style>
@endsection
