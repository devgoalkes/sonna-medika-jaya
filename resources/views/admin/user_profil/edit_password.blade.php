@extends('layouts.admin.template')
@section('title', 'Ubah Password')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li class="active">User</li>
	</ul>
</div>
@endsection

@section('content')
<ul class="tab-bar grey-tab">
	<li class="active">
		<a href="#overview">
			<span class="block text-center"><i class="fa fa-home fa-2x"></i></span>
			Overview
		</a>
	</li>
	<li>
		<a href="{{ route('admin.profil.edit', $user) }}">
			<span class="block text-center"><i class="fa fa-edit fa-2x"></i></span>
			Edit Profil
		</a>
	</li>
</ul>

<div class="padding-md">
	<div class="row">
		<div class="col-md-3 col-sm-3">
			<div class="row">
				<div class="col-xs-6 col-sm-12 col-md-6 text-center">
					<a href="#">
						@if(empty($cek->picture))
						<img src="{{ asset('admin/img/unknown.png') }}" alt="User Avatar" class="img-thumbnail">
						@else
						<img src="{{ asset('admin/img/'.$cek->picture) }}" alt="User Avatar" class="img-thumbnail">
						@endif
					</a>
					<div class="seperator"></div>
					<div class="seperator"></div>

				</div>
				<div class="col-xs-6 col-sm-12 col-md-6">
					<strong class="font-14">{{ $user->name }}</strong>
					<small class="block text-muted">{{ $user->email }}</small>
					<div class="seperator"></div>
					<a href="#" class="social-connect tooltip-test facebook-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
					<a href="#" class="social-connect tooltip-test twitter-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
					<a href="#" class="social-connect tooltip-test google-plus-hover pull-left" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a>
					<div class="seperator"></div>
					<div class="seperator"></div>
					<br>
					<button type="button" class="btn btn-info btn-xs btn-detail"  onclick="location.href='{{ route('admin.password.edit', $user) }}'">Ubah Password</button>
				</div>
			</div>
			@if($user->count() < 1)
			<div class="alert alert-danger">Anda belum melengkapi profil anda ! Klik <a href="{{ route('profil.edit', $user->id) }}">disini</a> untuk melengkapi profil anda.</div>
			@endif
		</div>
		<div class="col-md-9 col-sm-9">
			<div class="tab-content">
				<div class="tab-pane fade in active" id="overview">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default fadeInDown animation-delay2">
								<div class="panel-heading">
									<h4><b>Ubah Password</b></h4>
								</div>
								<div class="panel-body">

                  {!! Form::open(array('route' => ['admin.password.update', $user], 'method' => 'PUT', 'class' => 'form-horizontal form-border')) !!}

                    <div class="panel-body">
                      <div class="form-group">
                        <label class="control-label col-md-3">Password Lama</label>
                        <div class="col-md-5">
                          <input type="password" class="form-control input-sm" name="old_password" autocomplete="off" value="{{ old('old_password') }}">
                          <input type="hidden" name="id" value="{{ $user->id }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3">Password Baru</label>
                        <div class="col-md-5">
                          <input type="password" class="form-control input-sm" name="password" autocomplete="off" value="{{ old('password') }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3">Konfirmasi Password</label>
                        <div class="col-md-5">
                          <input type="password" class="form-control input-sm" name="password_confirmation" autocomplete="off" value="{{ old('password_confirmation') }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-5">
                          <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                          <button class="btn btn-sm btn-danger" type="reset" onclick="location.href='{{ route('admin.profil.index') }}'"><i class="fa fa-times"></i> Tutup</button>
                        </div>
                      </div>
                    </div>

                  {!! Form::close() !!}

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>
@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif
@endsection
