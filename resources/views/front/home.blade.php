@extends('layouts.front.template')
@section('front_title', 'Home')
@section('metatitle', "Home")
@section('metadescription', "The pioneer in the Copper Tube Industry.")
@section('content')

<section class="banner-area pt-100" id="home">
  <div class="container ">

    <div class=" ">
       <div id="featured" class="carousel slide carousel-fade justify-content-center" data-ride="carousel">
       <div class="carousel-inner">
         <div class="carousel-item active">
           <div class="text-white">
              <img class="img-fluid" src="{{ asset('storage/uploads/slides/1.jpg') }}" alt="">
            </div>
          </div>
          <div class="carousel-item">
            <!-- <div class="card bg-dark text-white"> -->
              <img class="img-fluid" src="{{ asset('storage/uploads/slides/2.jpg') }}" alt="">
            <!-- </div> -->
          </div>
          <div class="carousel-item">
            <!-- <div class="card bg-dark text-white"> -->
              <img class="img-fluid" src="{{ asset('storage/uploads/slides/3.jpg') }}" alt="">
            <!-- </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="row  d-flex align-items-center justify-content-center"> -->


      <!-- <div class="banner-content col-lg-12 col-md-12">
        <h1>
          Your Trusted Connection For Healthcare
        </h1>
        <form id="product-search" class="form-horizontal" action="{{ route('front.product.search') }}">
          <div class="input-group-icon mt-10">
						<input type="text" name="q" placeholder="Search our products" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search our products'" required class="single-input search-input">
            <button type="submit" class="icon"><i class="fa fa-search" aria-hidden="true"></i></button>
					</div>
        </form>
      </div> -->


    <!-- </div> -->
  </div>
</section>


<section class="price-area pt-40 pb-40">
	<div class="container">
    <div class="row d-flex align-items-center justify-content-center pt-20 pb-40">
      <div class="col-md-6">
        <form method="get" class="form-horizontal" action="{{ route('front.specialties.search') }}" role="search">
          {{ csrf_field() }}
          <div class="input-group-icon mt-10">
            <input type="text" name="q" placeholder="Search our specialties" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search our specialties'" required class="single-input-primary search-input">
            <button type="submit" class="icon"><i class="fa fa-search fa-lg" aria-hidden="true"></i></button>
          </div>
        </form>
      </div>
    </div>
    </div>
		<div class="row d-flex justify-content-center">
			<div class="menu-content pb-70 col-lg-8">
				<div class="title text-center">
					<h2 class="mb-10">Our Specialities</h2>
					<p>We come with genre products that you need</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<div class="single-price">
					<div class="top-part">
						<h1 class="package-no">
							<img class="mx-auto" src="front/img/pain.png" width="80px" height="80px" alt="">
						</h1>
						<h4>Pain Management</h4>
						<p>SPECIALTIES</p>
					</div>
					<div class="bottom-part">
						<a class="primary-btn text-uppercase" href="{{ route('front.specialty.category', 'pain-management') }}">View Detail</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="single-price">
					<div class="top-part">
						<h1 class="package-no">
							<img class="mx-auto" src="front/img/healt.png" width="80px" height="80px" alt="">
						</h1>
						<h4>Woman's Health Care</h4>
						<p>SPECIALTIES</p>
					</div>
					<div class="bottom-part">
						<a class="primary-btn text-uppercase" href="{{ route('front.specialty.category', 'womens-health-care') }}">View Detail</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="single-price">
					<div class="top-part">
						<h1 class="package-no">
							<img class="mx-auto" src="front/img/intensave.png" width="80px" height="80px" alt="">
						</h1>
						<h4>Intensive Care Systems</h4>
						<p>SPECIALTIES</p>
					</div>
					<div class="bottom-part">
						<a class="primary-btn text-uppercase" href="{{ route('front.specialty.category', 'intensive-care-systems') }}">View Detail</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="products-area pt-40 pb-40">
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center pb-40">
      <div class="col-md-6">
        <form method="get" class="form-horizontal" action="{{ route('front.product.search') }}">
          {{ csrf_field() }}
          <div class="input-group-icon mt-10">
            <input type="text" name="q" placeholder="Search our products" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search our products'" required class="single-input-primary search-input">
            <button type="submit" class="icon"><i class="fa fa-search fa-lg" aria-hidden="true"></i></button>
          </div>
        </form>
      </div>
    </div>
    <div class="row d-flex justify-content-center">

      <div class="col-md-12 pb-40 header-text text-center">
        <h1 class="pb-10">Best-Selling Products</h1>
        <p>
          We offers comprehensive medical equipment solutions, supported by experts in this industry.
        </p>
      </div>
    </div>
    <div class="row">
      @foreach($products as $product)
      <div class="col-lg-3 col-md-6">
        <div class="single-product">
          <div class="thumb">
            <img src="{{ asset('storage/uploads/products/'. $product->picture) }}" alt="{{ $product->name }}" width="100%" >
          </div>
          <div class="details">
            <h4 class="pb-10">{!! str_limit(strip_tags($product->brand), 20) !!}</h4>
            <h5>{!! str_limit(strip_tags($product->name), 22) !!}</h5>
            <a href="{{ route('front.product.detail', [$product->category,$product]) }}" class="primary-btn text-uppercase">View Details</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>

<section class="about-video-area pt-80">
  <div class="container">
    <div class="col-md-12 header-text text-center">
      <h1 class="pb-10">About Technology</h1>
    </div>
    <div class="row align-items-center">
      <div class="col-lg-6 about-video-left">
        <h2>
          A Vision for Precise Navigation
        </h2>
        <p>
          (Ultrasound for Pain Management)
          <ul>
            <li>WI Needle Intellegent Needle Visualization</li>
            <li>WI Guide Intelligent Needle Tracking - GPS</li>
            <li>WI Learn Education Center</li>
          </ul>
          <!-- SOMEDA as the sole distributor for Wisonic Medical Ultrasound systems, has been providing a total solution for Pain Treatment in Indonesia. We have also supported in educational effort through continuous medical education for the local Pain Physician society over the years. The pain physician team is also filled with experienced clinical specialist with medical nurse backgrounds, thus ensuring quality of service and products to this field of speciality.
          SOMEDA with Wisonic Medical Ultrasound Systems are your total solution partner in Pain Treatment and solving your pain physician needs. -->

          <!--Nearly any part of your body is vulnerable to pain. Acute pain warns us that something may be wrong. Chronic pain can rob us of our daily life, making it difficult and even unbearable. Many people with chronic pain can be helped by understanding the causes, symptoms, and treatments for pain - and how to cope with the frustrations.-->
        </p>
        <a class="primary-btn" href="{{ route('front.specialty.category', 'pain-management') }}">Get Details</a>
      </div>
      <div class="col-lg-6 about-video-right justify-content-center align-items-center d-flex relative">
        <div class="overlay overlay-bg"> <img src="{{ asset('video/navi.jpg') }}" height="100%" width="100%" alt=""> </div>
        <a class="play-btn" href="{{ asset('video/navi.mp4') }}"><img class="img-fluid mx-auto" src="template/assets/img/play-btn.png" alt=""></a>

        <!-- <iframe width="100%" height="100%" src="{{ asset('video/navi.mp4') }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
      </div>
    </div>
  </div>
</section>

<section class="about-video-area pt-80">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 about-video-right justify-content-center align-items-center d-flex relative">
        <div class="overlay overlay-bg"> <img src="{{ asset('video/lenshook.jpg') }}" height="100%" width="100%" alt=""> </div>
        <a class="play-btn" href="{{ asset('video/lenshook.mp4') }}"><img class="img-fluid mx-auto" src="template/assets/img/play-btn.png" alt=""></a>
        <!-- <iframe width="560" height="315" src="{{ asset('video/lenshook.mp4') }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
      </div>
      <div class="col-lg-6 home-about-right no-padding">
        <h2>Semen Quality Analyzer</h2>
        <p>
          (LensHook X1 Pro)
          <ul>
            <li>Fully Automatic</li>
            <li>3 Steps Operator</li>
            <li>HDMI Video Output</li>
            <li>Operator Independent</li>
          </ul>
          <!-- SOMEDA as a distributor in Indonesia, distributes medical devices for the women’s healthcare industry including high quality equipment and medical diagnostics. Our focus is to improve the quality of life for female patients by offering the best medical device technology -->
          <!--Delivering unmatched performance in women’s ultrasound applications, Mindray ultrasound products are designed to improve your clinical capacity in obstetric, gynecologic, and breast imaging.-->
        </p>
        <a class="primary-btn" href="{{ route('front.specialty.category', 'womens-health-care') }}">Get Details</a>
      </div>
    </div>
  </div>
</section>

<section class="about-video-area pt-80 pb-100">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 about-video-left">
        <h2>
          Early Diagnosis of Osteoporosis
        </h2>
        <p>
          1st Technology with REMS Method
          <!-- With some professional manufacture in Intensive Care Systems, SOMEDA as a distributor for Northern Meditec bring to the market for provide a solution in Operating Room, Emergency Room and Intensive Care Unit (ICU/NICU/PICU). Range of product we can offering such as Patient Monitor, Anesthesia Machine, Ventilator ICU, Operating Table & Operating Light, Infant Incubator, Infant Radiant Warmerand others related products.
          Our team consist of specialized people trained to help Medical Technologist in various filed with depth technical knowledge, offering hospitals a full solution in advancing their services. -->
        </p>
        <a class="primary-btn" href="{{ route('front.specialty.category', 'intensive-care-systems') }}">Get Details</a>
      </div>
      <div class="col-lg-6 about-video-right justify-content-center align-items-center d-flex relative">
        <!-- <iframe width="100%" height="100%" src="https://www.youtube.com/embed/3R5nuRCZQIs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
        <div class="overlay overlay-bg"><img src="{{ asset('video/echolight.jpg') }}" height="100%" width="100%" alt=""></div>
        <a class="play-btn" href="{{ asset('video/echolight.mp4') }}"><img class="img-fluid mx-auto" src="template/assets/img/play-btn.png" alt=""></a>
<!--
        <iframe width="100%" height="100%" src="{{ asset('video/echolight.mp4') }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
      </div>
    </div>
  </div>
</section>

<section class="feature-area pt-80 pb-80">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h1 class="pb-10 text-white">Our Newest Events</h1>
        <p class="text-white">Our participation in various events within the scope of health and medical equipment products</p>
			</div>
		</div>
    <div class="row">
			@forelse($events as $event)
			<div class="col-lg-4 col-md-6">
				<div class="single-feature">
					<div class="thumb">
						<img src="{{ asset('storage/uploads/events/'. $event->picture) }}" alt="" width="100%">
					</div>
					<br>
					<a href="{{ route('front.event.detail', $event) }}" class="title d-flex flex-row">
						<h4>{{ $event->title }}</h4>
					</a>
					<p>
						{!! str_limit(strip_tags($event->content), 100) !!}
					</p>
				</div>
			</div>
			@empty
			<div class="col-lg-12 col-md-12">
				<div class="single-feature">
					<p>
						No Events available.
					</p>
				</div>
			</div>
			@endforelse
		</div>
	</div>
</section>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('front/css/flexslider.css') }}">
@endsection

@push('scripts')
<script src="{{ asset('front/js/flexslider.js') }}"></script>
<script type="text/javascript">
  $('.flexslider').flexslider({
      animation: "fade",
      slideshowSpeed: 2000,
      slideshow: true,
      animationSpeed :500,
      touch: true,
      pauseOnHover: true,
      controlNav:true
  });

@endpush
