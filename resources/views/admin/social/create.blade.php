@extends('layouts.admin.template')
@section('title', 'Tambah Sosial Media')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li><a onclick="location.href='{{ route('admin.social.index') }}'">Sosial Media</a></li>
		 <li class="active">Add</li>
	</ul>
</div>
@endsection
@section('content')

<div class="panel panel-primary">
	<div class="panel-heading">Tambah Akun Sosial Media</div>
	<div class="panel-body">
		{!! Form::open(array('route' => 'admin.social.store', 'methode' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) !!}

			<div class="form-group">
        <label for="album_id" class="col-sm-2 control-label">Sosial Media</label>
        <div class="col-sm-5">
          <select class="form-control" name="driver">
							<option value="facebook"> Facebook</option>
            	<option value="twitter"> Twitter</option>
							<option value="youtube"> Youtube</option>
							<option value="instagram"> Instagram</option>
							<option value="pinterest"> Pinterest</option>
							<option value="path"> Path</option>
          </select>
        </div>
      </div>
			<div class="form-group">
				<label for="username" class="col-sm-2 control-label">Username</label>
				<div class="col-sm-10">
					<input type="text" name="username" class="form-control" id="username" placeholder="Username Sosial Media Anda" autocomplete="off">
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
					<a onclick="location.href='{{ route('admin.social.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
				</div>
			</div>

		{!! Form::close() !!}

	</div>
</div>
@endsection


@section('style')
<link href="{{ asset('admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('admin/js/jquery.gritter.min.js') }}"></script>

@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif
@endsection
