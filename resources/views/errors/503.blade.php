@extends('layouts.front.template')
@section('title', "503 Maintenance Site")
@section('metatitle', "503 Maintenance Site")
@section('metadescription', "503 Maintenance Site")
@section('content')
<section class="blog-posts-area pt-120 mt-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 post-list blog-post-list text-center">
				<div class="single-post">
					<a href="#">
						<h2>503!</h2>
					</a>
          <hr>
					<div class="content-wrap">

            <h5>Site Under Maintenance/h5>
            <p>Please visit us later, we'll be back shortly!</p>
            <a href="{{ url('/') }}" class="default-btn">Back to Home</a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
