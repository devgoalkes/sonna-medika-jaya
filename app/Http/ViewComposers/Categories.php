<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Category;

class Categories
{

  	public $categories;


  	function __construct(Category $categories)
  	{
  		$this->categories = Category::where('active', 'Y')->where('slug', 'article')->first();
  	}

  	public function compose(View $view)
  	{
  		$view->with('categories', $this->categories);
  	}
}
