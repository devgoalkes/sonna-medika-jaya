<div class="panel blog-container">
	<div class="panel-body">
		<h4>{{ $event->title }}</h4>
		<small class="text-muted">Tanggal : {{ tglIndo($event->created_at) }} </small>
		<div class="seperator"></div>
    <div class="input-group">
				<input id="slug" type="text" value="{{ 'pages/'.$event->slug }}" class="form-control">
				<span class="input-group-btn">
					<button class="btn btn-success copy-button" data-clipboard-action="copy" data-clipboard-target="#slug">Copy</button>
				</span>
			</div>
		<div class="seperator"></div>
		@if( $event->picture != '' )
		<img width="200px" class="img-thumbnail" style="float:left; margin-right:10px" src="{{ url('storage/uploads/page/'.$event->picture) }}" alt="{{ $event->title }}">
		@endif
		{!! $event->content !!}

	</div>
</div>
