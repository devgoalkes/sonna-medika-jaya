@if(empty($contact->id))
	{{ $contact }}
@else
<div class="panel blog-container">
	<div class="panel-body">
		<h4>{{ $contact->subject }}</h4>
		<small class="text-muted">Oleh : <a href="#"><strong> {{ $contact->name }}</strong></a> |  Tanggal : {{ tglIndo($contact->created_at) }}</small>
		<div class="seperator"></div>
		<div class="seperator"></div>
		{!! $contact->message !!}
	</div>
</div>
@endif
