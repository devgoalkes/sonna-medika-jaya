@extends('layouts.admin.template')
@section('title', 'Add Category')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li></i><a onclick="location.href='{{ route('admin.category.index') }}'">Category</a></li>
		 <li class="active">Add</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">&nbsp;</div>
	<div class="panel-body">
		{!! Form::open(['route' => 'admin.category.store', 'methode' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}

		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Category Name</label>
			<div class="col-sm-5">
				<input type="text" name="name" class="form-control" id="name" placeholder="Category Name" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label for="parent_id" class="col-sm-2 control-label">Parent</label>
			<div class="col-sm-5">
				<select class="form-control" name="parent_id">
					<option value="0">No Parent</option>
						@forelse($parent_menus as $r)
							<option value="{!! $r->id !!}">{!! $r->name !!}</option>
						@empty
							<option value="0">No Parent</option>
						@endforelse
				</select>
			</div>
		</div>
		<div class="form-group">
			<label for="status" class="col-sm-2 control-label">Status</label>
			<div class="col-lg-10">
				<label class="label-radio inline">
					<input type="radio" name="status" value="Y">
					<span class="custom-radio"></span>
					Active
				</label>
				<label class="label-radio inline">
					<input type="radio" name="status" value="N">
					<span class="custom-radio"></span>
					Non Active
				</label>
			</div>
		</div>
		<div class="form-group">
			<label for="picture" class="col-sm-2 control-label">Picture</label>
			<div class="col-sm-5">
				<div class="upload-file">
					<input type="file" name="picture" id="picture" class="picture">
					<label data-title="Select file" for="picture">
						<span data-title="No file selected..."></span>
					</label>
				</div>
				<br>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a onclick="location.href='{{ route('admin.category.index') }}'" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/gritter/jquery.gritter.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/jquery.gritter.min.js') }}"></script>
<script>
	$('.picture').change(function()	{
		var filename = $(this).val().split('\\').pop();
		$(this).parent().find('span').attr('data-title',filename);
		$(this).parent().find('label').attr('data-title','Change file');
		$(this).parent().find('label').addClass('selected');
	});
</script>
@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <script>
			$.gritter.add({
				title: '<i class="fa fa-warning"></i> Error',
				text: '{{ $error }}',
				sticky: false,
				time: '5000',
				class_name: 'gritter-danger'
			});
        </script>
    @endforeach
@endif
@endsection
