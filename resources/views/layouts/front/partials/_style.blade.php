<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('front/css/linearicons.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/nice-select.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/hexagons.min.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/main.css') }}">
  @yield('style')
