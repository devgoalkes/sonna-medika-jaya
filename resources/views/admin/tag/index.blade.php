@extends('layouts.admin.template')
@section('title', 'Tag')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li class="active">Tag</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">&nbsp;</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{route('admin.tag.create')}}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add @yield('title') </a>
		@endsection
		{!! $html->table(['class' => 'table table-striped']) !!}
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('asset_admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('asset_admin/js/sweetalert.min.js') }}"></script>
<script>
    $('button.delete_button').on('click', function(e){
    	e.preventDefault();
    	var self = $(this);
    	swal({
        title: "Alert",
        text: "Apakah anda yakin ingin menghapus data ini ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#cc3f44",
        confirmButtonText: "Hapus",
        cancelButtonText : "Batal",
        closeOnConfirm: false,
        closeOnCancel: false
    	},
    	function(isConfirm) {
	        if(isConfirm){
	        	self.parents(".delete_form").submit();
	        }else{
	        	swal('Cancelled', 'Data anda gagal dihapus !', 'error');
	        }
	    });
    });
</script>
{!! $html->scripts() !!}
@endsection
