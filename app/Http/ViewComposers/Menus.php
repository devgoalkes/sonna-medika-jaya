<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Menu;

class Menus
{

  	public $categories;


  	function __construct(Menu $menus)
  	{
      $this->menus = $menus->with([
  			'submenu'
  		])->where(['active' => 'Y', 'admin' => 'N', 'id_parent' => 0])->get();
  	}

  	public function compose(View $view)
  	{
  		$view->with('menus', $this->menus);
  	}
}
