<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $menus = [
              		[
              			"id" => 1,
              			"id_parent" => "0",
              			"name" => "Home",
              			"url" => "admin/",
              			"icon" => "fa-home",
              			"admin" => "Y",
              			"active" => "Y"
              		],
              		[
              			"id" => 2,
              			"id_parent" => "0",
              			"name" => "Tentang Kami",
              			"url" => "contact-us",
              			"icon" => "fa-user-circle-o",
              			"admin" => "N",
              			"active" => "Y"
              		],
              		[
              			"id" => 3,
              			"id_parent" => "0",
              			"name" => "Content",
              			"url" => "#",
              			"icon" => "fa-newspaper-o",
              			"admin" => "Y",
              			"active" => "Y"
              		],
              		[
              			"id" => 4,
              			"id_parent" => "3",
              			"name" => "Category",
              			"url" => "admin/category",
              			"icon" => "fa-newspaper-o",
              			"admin" => "Y",
              			"active" => "Y"
              		],
              		[
              			"id" => 5,
              			"id_parent" => "3",
              			"name" => "Article",
              			"url" => "admin/article",
              			"icon" => "fa-newspaper-o",
              			"admin" => "Y",
              			"active" => "Y"
              		],
              		[
              			"id" => 6,
              			"id_parent" => "3",
              			"name" => "Page",
              			"url" => "admin/page",
              			"icon" => "fa-newspaper-o",
              			"admin" => "Y",
              			"active" => "Y"
              		],
              		[
              			"id" => 8,
              			"id_parent" => "0",
              			"name" => "Setting",
              			"url" => "admin/setting",
              			"icon" => "fa-gears",
              			"admin" => "Y",
              			"active" => "Y"
              		]
              	];

        foreach ($menus as $menu) {
            DB::table('menus')->insert([
                'id' => $menu['id'],
                'id_parent' => $menu['id_parent'],
                'name' => $menu['name'],
                'url' => $menu['url'],
                'icon' => $menu['icon'],
                'admin' => $menu['admin'],
                'active' => $menu['active']
            ]);
        }
    }
}
