<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'old_password'          => 'required',
          'password'              => 'min:3|different:old_password',
          'password_confirmation' => 'required|same:password'
        ];
    }

    public function messages()
    {
        return [
            'old_password.required' => 'Password lama wajib diisi !',
            'password.min' => 'Password minimal :min karakter !',
            'password.different' => 'Password baru tidak boleh sama dengan password lama !',
            'password_confirmation.required' => 'Konfirmasi password wajib diisi !',
            'password_confirmation.same' => 'Konfirmasi password tidak cocok !',

        ];
    }
}
