<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{ Category, Article };

class ArticleController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $articles = Article::where('active', 'Y')->orderBy('created_at', 'desc')->paginate(10);
        return view('front.article.index', compact('categories', 'articles'));
    }

    public function category(Category $category)
		{
        return view('front.article.category', compact(['category']));
		}

    public function detail(Category $category, Article $article)
		{
        $categories = Category::where('active', 'Y')->get();
        return view('front.article.detail', compact(['article', 'categories']));
		}
}
