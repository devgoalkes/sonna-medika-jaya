@extends('layouts.front.template')
@section('metatitle', "Our Events")
@section('metadescription', "Our Events")
@section('title', 'Our Events')

@section('content')
<section class="feature-area section-gap">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h1 class="pb-10 text-white">Our Event</h1>
				<p class="text-white">
					Who are in extremely love with eco friendly system.
				</p>
			</div>
		</div>
		<div class="row">
			@forelse($events as $event)
			<div class="col-lg-4 col-md-6">
				<div class="single-feature">
					<div class="thumb">
						<img src="{{ asset('storage/uploads/events/'. $event->picture) }}" alt="" width="100%">
					</div>
					<br>
					<a href="{{ route('front.event.detail', $event) }}" class="title d-flex flex-row">
						<h4>{{ $event->title }}</h4>
					</a>
					<p>
						{!! str_limit(strip_tags($event->content), 100) !!}
					</p>
				</div>
			</div>
			@empty
			<div class="col-lg-12 col-md-12">
				<div class="single-feature">
					<p>
						Usage of the Internet is becoming more common due to rapid advancement of technology and power.
					</p>
				</div>
			</div>
			@endforelse
		</div>
	</div>
</section>
@endsection
