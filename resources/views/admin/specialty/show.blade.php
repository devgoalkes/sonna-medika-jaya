<div class="panel blog-container">
	<div class="panel-body">
		<h4>{{ $specialty->title }}</h4>
		<small class="text-muted">Tanggal : {{ tglIndo($specialty->created_at) }} </small>
		<div class="seperator"></div>
    <div class="input-group">
				<input id="slug" type="text" value="{{ 'specialty/'.$specialty->slug }}" class="form-control">
				<span class="input-group-btn">
					<button class="btn btn-success copy-button" data-clipboard-action="copy" data-clipboard-target="#slug">Copy</button>
				</span>
			</div>
		<div class="seperator"></div>
		@if($specialty->picture)
		<img width="200px" class="img-thumbnail" style="float:left; margin-right:10px" src="{{ url('storage/uploads/specialty/'.$specialty->picture) }}" alt="{{ $specialty->title }}">
		@else
		<img width="200px" class="img-thumbnail" style="float:left; margin-right:10px" src="{{ url('images/goalkes_article.jpg') }}" alt="{{ $specialty->title }}">
		@endif
		{!! $specialty->content !!}

	</div>
</div>
