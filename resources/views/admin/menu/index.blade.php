@extends('layouts.admin.template')
@section('title', 'Menu')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li class="active">Menu</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary">
	<div class="panel-heading">
		&nbsp;
	</div>
	<div class="panel-body table-responsive">
		@section('button')
		<a onclick="location.href='{{ route('admin.menu.create') }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add Menu</a>
		@endsection
		<div class="row">
			<div class="col-md-12">
				{!! $html->table(['class' => 'table table-striped']) !!}
			</div>
		</div>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('asset_admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('asset_admin/js/sweetalert.min.js') }}"></script>

<script type="text/javascript">
		function konfirm(id){
				var id = id.getAttribute('data-id');
				swal({
						title: "Are you sure?",
						text: "You will not be able to recover this data file!",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, delete it!",
						closeOnConfirm: false,
						showLoaderOnConfirm: true
				},
				function(){
						$.ajax({
								type: "POST",
								url: '{{route('admin.menu.index')}}/' + id,
								data: {
										'_token': "{{ csrf_token() }}",
										'_method' : 'DELETE'
								},
								success: function() {
										$('#dataTableBuilder').DataTable().ajax.reload();
										swal("Deleted!", "Main Banner Berhasil Dihapus!", "success");
								},
								error: function() {
										swal("Error!", "Main Banner Gagal Dihapus!", "error");
								}
						});
				});
		}
</script>
{!! $html->scripts() !!}
@endsection
