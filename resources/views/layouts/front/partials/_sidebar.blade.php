<div class="col-lg-4 sidebar">
	<div class="single-widget category-widget">
		<h4 class="title">Post Categories</h4>
		<ul>
			@foreach($categories->subcategory as $category)
			<li><a href="#" class="justify-content-between align-items-center d-flex"><h6>{{ $category->name }}</h6> <span>{{ $category->article }}</span></a></li>
			@endforeach
		</ul>
	</div>

	<div class="single-widget recent-posts-widget">
		<h4 class="title">Recent Posts</h4>
		<div class="blog-list ">
			@foreach($latest as $article)
			<div class="single-recent-post d-flex flex-row">
				<div class="recent-thumb">
					<img class="img-fluid" src="{{ asset('storage/uploads/articles'. $article->picture) }}" alt="">
				</div>
				<div class="recent-details">
					<a href="{{ route('front.article.detail', $article) }}">
						<p>
							<h4>{{ $article->title }}</h4>
							<small>{{ tglIndo($article->created_at) }}</small>
						</p>
					</a>

				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
