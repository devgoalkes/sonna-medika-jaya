@extends('layouts.admin.template')
@section('title', 'Profil User')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li class="active">User</li>
	</ul>
</div>
@endsection

@section('content')
<ul class="tab-bar grey-tab">
	<li class="active">
		<a href="#overview">
			<span class="block text-center"><i class="fa fa-home fa-2x"></i></span>
			Overview
		</a>
	</li>
	<li>
		@if($user->user_profile()->count() > 0)
		<a href="{{ route('admin.profil.edit', $user) }}">
			<span class="block text-center"><i class="fa fa-edit fa-2x"></i></span>
			Edit Profile
		</a>
		@else
		<a href="{{ route('admin.profil.create') }}">
			<span class="block text-center"><i class="fa fa-edit fa-2x"></i></span>
			Edit Profile
		</a>
		@endif
	</li>
</ul>

<div class="padding-md">
	<div class="row">
		<div class="col-md-3 col-sm-3">
			<div class="row">
				<div class="col-xs-6 col-sm-12 col-md-6 text-center">
					<a href="#">
						@if(empty($user->user_profile->picture))
						<img src="{{ asset('asset_admin/img/unknown.png') }}" alt="User Avatar" class="img-thumbnail">
						@else
						<img src="{{ asset('asset_admin/img/'. $user->user_profil->user_profile->picture) }}" alt="User Avatar" class="img-thumbnail">
						@endif
					</a>
					<div class="seperator"></div>
					<div class="seperator"></div>

				</div>
				<div class="col-xs-6 col-sm-12 col-md-6">
					<strong class="font-14">{{ $user->name }}</strong>
					<small class="block text-muted">{{ $user->email }}</small>
					<div class="seperator"></div>
					<a href="#" class="social-connect tooltip-test facebook-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
					<a href="#" class="social-connect tooltip-test twitter-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
					<a href="#" class="social-connect tooltip-test google-plus-hover pull-left" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a>
					<div class="seperator"></div>
					<div class="seperator"></div>
					<br>
					<button type="button" class="btn btn-info btn-xs btn-detail"  onclick="location.href='{{ route('admin.password.edit', $user) }}'">Ubah Password</button>
				</div>
			</div>
			@if($user->user_profile()->count() < 1)
			<div class="alert alert-danger">Anda belum melengkapi profil anda ! Klik <a href="{{ route('admin.profil.create') }}">disini</a> untuk melengkapi profil anda.</div>
			@endif
		</div>
		<div class="col-md-9 col-sm-9">
			<div class="tab-content">
				<div class="tab-pane fade in active" id="overview">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default fadeInDown animation-delay2">
								<div class="panel-heading">
									<h4><b>My Profile</b></h4>
								</div>
								<div class="panel-body">
									<p>
										<table class="table table-striped">
											<tr><td width="175px">Full Name</td> <td>{{ empty($user->user_profile->full_name) ? '' : $user->user_profile->full_name }}</td></tr>
											<tr><td>Gender</td>            <td>{{ empty($user->user_profile->sex) ? '' : $user->user_profile->sex }}</td></tr>
											<tr><td>Phone Number</td>      <td>{{ empty($user->user_profile->phone_number) ? '' : $user->user_profile->phone_number }}</td></tr>
											<tr><td>Address</td>           <td>{{ empty($user->user_profile->address) ? '' : $user->user_profile->address }}</td></tr>
											<tr><td>Google</td>            <td>{{ empty($user->user_profile->google) ? '' : $user->user_profile->google }}</td></tr>
											<tr><td>Facebook</td>          <td>{{ empty($user->user_profile->facebook) ? '' : $user->user_profile->facebook}}</td></tr>
											<tr><td>Twitter</td>           <td>{{ empty($user->user_profile->twitter) ? '' : $user->user_profile->twitter }}</td></tr>
											<tr><td>Youtube</td>           <td>{{ empty($user->user_profile->youtube) ? '' : $user->user_profile->youtube  }}</td></tr>
											<tr><td>Path</td>              <td>{{ empty($user->user_profile->path) ? '' : $user->user_profile->path }}</td></tr>
											<tr><td>Description</td>       <td>{{ empty($user->user_profile->description) ? '' : $user->user_profile->description }}</td></tr>
										</table>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body" style="max-height: calc(100vh - 100px); overflow-y: auto;">
        <div id="modalcontent"></div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/sweetalert.min.js') }}"></script>
@endsection
