@extends('layouts.front.template')
@section('title', "Category ". $category->name)
@section('metatitle', "Category ". $category->name)
@section('metadescription', "Category ". $category->name)
@section('image', asset('/images/no-picture.jpg'))

@section('content')
<main class="main-content">
  <div class="bredcrumbWrap">
		<div class="container">
			<nav class="breadcrumb">
        <a href="{{ route('home') }}">Home</a>
        <span aria-hidden="true">&rsaquo;</span>
        <a href="#">Kategori</a>
        <span aria-hidden="true">&rsaquo;</span>
        <span>{{ $category->name }}</span>
      </nav>
		</div>
	</div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
				<h2 class="h3">{{ $category->name }}</h2>
				<hr>



        <div class="row">
            <div class="col-lg-12">
                <div class="blog-items grid-list row">
                    @forelse($category->articles as $article)
                    <div class="col-md-4 col-sm-6 padding-15">
                        <div class="blog-post">
                            <img src="{{ asset('front/img/post-1.jpg') }}" alt="blog post">
                            <div class="blog-content">
                                <span class="date"><i class="fa fa-clock-o"></i> January 01.2018</span>
                                <h3><a href="{{ route('article.detail', [$article->category, $article]) }}">{{ $article->title }}</a></h3>
                                <p>{!! str_limit(strip_tags($article->content), 200) !!}</p>
                                <a href="#" class="post-meta">Read More</a>
                            </div>
                        </div>
                    </div>
                    @empty
                    Tidak ada berita
                    @endforelse
                </div>
                <ul class="pagination_wrap align-center mt-30">
                    <li><a href="#"><i class="ti-arrow-left"></i></a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#" class="active">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#"><i class="ti-arrow-right"></i></a></li>
                </ul>
            </div>
        </div>




      </div>
    </div>
  </div>
</main>
@endsection

@section('styles')
<style media="screen">
  .blog-row {margin-top:50px; }
  .margin_bottom30 { margin-bottom:30px; }
  .margin_left10 {margin-left:10px; }
  .blog-content {padding:10px; }
  .bg-white {background-color:#fff;}
</style>
@endsection
