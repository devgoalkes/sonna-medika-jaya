@extends('layouts.front.template')
@section('title', "403 Access Denied")
@section('metatitle', "403 Access Denied")
@section('metadescription', "403 Access Denied")
@section('content')
<section class="blog-posts-area pt-120 mt-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 post-list blog-post-list text-center">
				<div class="single-post">
					<a href="#">
						<h2>403!</h2>
					</a>
          <hr>
					<div class="content-wrap">

            <h5>403 Access Denied</h5>
            <p>You're don't have permission to access / on this server.</p>
            <a href="{{ url('/') }}" class="default-btn">Back to Home</a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
