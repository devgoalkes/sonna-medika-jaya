@extends('layouts.admin.template')
@section('title', 'Website Setting')
@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li class="active">Setting</li>
	</ul>
</div>
@endsection
@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">&nbsp;</div>
	<div class="padding-md clearfix">
		@section('button')
		<a onclick="location.href='{{ route('admin.setting.edit', $setting->id) }}'" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Edit</a>
		@endsection
    <table class="table table-striped">
      <tr><td width="175px">Website Name</td> <td>{{ $setting->website_name }}</td></tr>
      <tr><td>Website Address</td>            <td>{{ $setting->website_address }}</td></tr>
      <tr><td>Meta Description</td>           <td>{{ $setting->meta_description }}</td></tr>
      <tr><td>Meta Keyord</td>                <td>{{ $setting->meta_keyword }}</td></tr>
      <tr><td>Favicon</td>                    <td>{{ $setting->favicon }}</td></tr>
      <tr><td>Mail Address</td>               <td>{{ $setting->mail_address }}</td></tr>
      <tr><td>Address</td>                    <td>{{ $setting->address }}</td></tr>
      <tr><td>Phone</td>                      <td>{{ $setting->phone }}</td></tr>
    </table>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('asset_admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('asset_admin/js/sweetalert.min.js') }}"></script>
@endsection
