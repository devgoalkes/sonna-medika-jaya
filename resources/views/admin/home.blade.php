@extends('layouts.admin.template')
@section('title', 'Dashboard')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li class="active"><i class="fa fa-home"></i> Home</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary table-responsive">
  <div class="panel-heading">&nbsp;</div>
	<div class="well alert-danger">Selamat datang di halaman Administrator Website {{ config('app.name') }}</div>
	<div class="grey-container shortcut-wrapper">
			<a href="{{ route('admin.user.index') }}" class="shortcut-link">
				<span class="shortcut-icon">
					<i class="fa fa-user"></i>
					<span class="shortcut-alert">{{ $users }}</span>
				</span>
				<span class="text">Users</span>
			</a>
			<a href="{{ route('admin.article.index') }}" class="shortcut-link">
				<span class="shortcut-icon">
					<i class="fa fa-question"></i>
					<span class="shortcut-alert">{{ $articles }}</span>
				</span>
				<span class="text">Article</span>
			</a>
			<a href="{{ route('admin.setting.index') }}" class="shortcut-link">
				<span class="shortcut-icon">
					<i class="fa fa-cog"></i></span>
				<span class="text">Setting</span>
			</a>
		</div>
</div>
@endsection
