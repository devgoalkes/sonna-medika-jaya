@extends('layouts.front.template')
@section('title', 'Article Goalkes')
@section('metatitle', "Pusat Bantuan")
@section('metadescription', "Pusat Bantuan")
@section('image', asset('/images/goalkes.jpg'))

@section('content')
<main class="main-content">
  <div class="bredcrumbWrap">
		<div class="container">
			<nav class="breadcrumb"> &nbsp;</nav>
		</div>
	</div>
  <div class="container">
    <div class="row">
      <div class=" col-md-4 sidebar">
        @include('layouts.front.partials._sidebar')
			</div>

      @forelse($category->articles as $article)
      <div class="col-md-8 col-sm-8 col-xs-12 margin_bottom30">
        <div class="thumbnail">
  			<a href="javascript::;">
  				<img class="img-responsive center-block" src="http://i50.tinypic.com/2nbf0ht.jpg" height="250">
  				</a>
  				<div class="blog-content">
    				<h3><a href="#">{{ str_limit($article->title, 25) }}</a></h3>
    				<p>Category : <a href="javascript::;">Nature</a></p>
    				<p class="text-justify">{{ str_limit(strip_tags($article->content), 130) }}<a href="javascript::;"  class="heading_color">  Continue Reading</a></p>
    				<hr class="hr--small">
    				<p><span>Share :
    				<a href="javascript::;"><i class="fa fa-facebook margin_left10" aria-hidden="true"></i></a>
    				<a href="javascript::;"><i class="fa fa-twitter margin_left10" aria-hidden="true"></i></a>
    				<a href="javascript::;"><i class="fa fa-google-plus margin_left10" aria-hidden="true"></i></a>
    				 </span>
    				<span class="pull-right">By : <strong>Blogger</strong></span> </p>
  				</div>
  			</div>
      </div>
      @empty
      <div class="col-md-12 margin_bottom30">
        <div class="panel panel-default">
          <div class="panel-heading text-center">
            <h3>Maaf belum ada produk dalam category {{ $category->name }}.</h3>
          </div>
        </div>
      </div>

      @endforelse
    </div>
  </div>
</main>
@endsection

@section('styles')
<style media="screen">
  .blog-row {margin-top:50px; }
  .margin_bottom30 { margin-bottom:30px; }
  .margin_left10 {margin-left:10px; }
  .blog-content {padding:10px; }
  .bg-white {background-color:#fff;}
</style>
@endsection
