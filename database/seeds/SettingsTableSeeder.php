<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('settings')->insert([
          "id" => 1,
          "website_name" => "PT. Sonna Medik Jaya",
          "website_address" => "-",
          "meta_description" => "-",
          "meta_keyword" => "-",
          "favicon" => "-",
          "mail_address" => "info@sonnamedika.com",
          "address" => "Rukan Sentra Niaga, Blok M-17 GreenLake City,West of Jakarta Indonesia",
          "phone" => "081234567890",
          "created_at" => Carbon::now(),
          "updated_at" => null
      ]);
    }
}
