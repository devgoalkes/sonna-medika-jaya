@extends('layouts.admin.template')
@section('title', 'Edit User')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
     <li>User</li>
     <li>Profil</li>
		 <li class="active">Edit</li>
	</ul>
</div>
@endsection

@section('content')

<ul class="tab-bar grey-tab">
	<li>
		<a href="{{ route('admin.profil.index') }}">
			<span class="block text-center"><i class="fa fa-home fa-2x"></i></span>
			Overview
		</a>
	</li>
	<li class="active">
		<a href="">
			<span class="block text-center"><i class="fa fa-edit fa-2x"></i></span>
			Edit Profile
		</a>
	</li>
</ul>

<div class="padding-md">
	<div class="row">
		<div class="col-md-3 col-sm-3">
			<div class="row">
				<div class="col-xs-6 col-sm-12 col-md-6 text-center">
					<a href="#">
						<img src="{{ asset('asset_admin/img/unknown.png') }}" alt="User Avatar" class="img-thumbnail">
					</a>
					<div class="seperator"></div>
					<div class="seperator"></div>
				</div>
				<div class="col-xs-6 col-sm-12 col-md-6">
					<strong class="font-14">{{ $user->name }}</strong>
					<small class="block text-muted">{{ $user->email }}</small>
					<div class="seperator"></div>
					<div class="seperator"></div>
					<a href="#" class="social-connect tooltip-test facebook-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a>
					<a href="#" class="social-connect tooltip-test twitter-hover pull-left m-right-xs" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
					<a href="#" class="social-connect tooltip-test google-plus-hover pull-left" data-toggle="tooltip" data-original-title="Google Plus"><i class="fa fa-google-plus"></i></a>
					<div class="seperator"></div>
					<div class="seperator"></div>
				</div>
			</div>
		</div>
    <div class="col-md-9 col-sm-9">
			<div class="tab-content">
				<div class="tab-pane fade in active" id="overview">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default fadeInDown animation-delay2">
								<div class="panel-heading">
									<h4><b>Edit Profile</b></h4>
								</div>
								<div class="panel-body">
									{!! Form::open(array('route' => 'admin.profil.store', 'methode' => 'POST', 'class' => 'form-horizontal form-border')) !!}
										<div class="panel-body">
											{{ $user->user_profil }}
											<div class="form-group">
												<label class="control-label col-md-2">Full Name</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-sm" name="full_name" autocomplete="off" value="">
									        <input type="hidden" name="id" value="{{ $user->id }}">
												</div>
											</div>
									    <div class="form-group">
												<label class="control-label col-md-2">Gender</label>
												<div class="col-md-10">
													<label class="label-radio inline">
														<input type="radio" name="sex" value="L">
														<span class="custom-radio"></span>
														Male
													</label>
													<label class="label-radio inline">
														<input type="radio" name="sex" value="P">
														<span class="custom-radio"></span>
														Female
													</label>
												</div>
											</div>
									    <div class="form-group">
									      <label class="control-label col-md-2">Address</label>
									      <div class="col-md-10">
									        <textarea class="form-control" rows="3" name="address"></textarea>
									      </div>
									    </div>
											<div class="form-group">
												<label class="control-label col-md-2">Phone</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-sm" name="phone_number" autocomplete="off">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-2">Twitter</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-sm" name="twitter">
												</div>
											</div>
									    <div class="form-group">
												<label class="control-label col-md-2">Google</label>
												<div class="col-md-10">
													<input type="text" class="form-control input-sm" name="google">
												</div>
											</div>
									    <div class="form-group">
									      <label class="control-label col-md-2">Youtube</label>
									      <div class="col-md-10">
									        <input type="text" class="form-control input-sm" name="youtube">
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-md-2">Path</label>
									      <div class="col-md-10">
									        <input type="text" class="form-control input-sm" name="path">
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-md-2">Description</label>
									      <div class="col-md-10">
									        <textarea class="form-control" rows="3" name="description"></textarea>
									      </div>
									    </div>
										</div>
										<div class="panel-footer">
											<div class="text-right">
												<button class="btn btn-sm btn-success" type="submit"><i class="fa fa-save"></i> Save</button>
												<button class="btn btn-sm btn-danger" type="button" onclick="location.href='{{ url('adm/profil') }}'"><i class="fa fa-times"></i> Close</button>
											</div>
										</div>
									{!! Form::close() !!}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </div>
</div>
@endsection

@section('js')
<script src="{{ asset('admin/js/sweatalert.js') }}"></script>
@endsection
