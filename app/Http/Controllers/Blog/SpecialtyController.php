<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{ Category, Specialty };

class SpecialtyController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $specialties = Specialty::where('active', 'Y')->orderBy('created_at', 'desc')->paginate(10);
        return view('front.specialty.index', compact('categories', 'specialties'));
    }

    public function category(Category $category)
		{
        
        return view('front.specialty.category', compact(['category']));
		}

    public function detail(Category $category, Specialty $specialty)
		{
        return view('front.specialty.detail', compact(['specialty', 'category']));
		}
}
