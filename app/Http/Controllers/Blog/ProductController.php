<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{ Product, Category };
use Ramsey\Uuid\Uuid;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::where('active', 'Y')->get();
        return view('front.product.index', compact('products'));
    }

    public function category(Category $category)
		{
  			$products = $category->products()->where('active', 'Y')->orderBy('created_at', 'desc')->paginate(10);
  			return view('front.product.category', compact(['products', 'category']));
		}

    public function detail(Category $category, Product $product)
    {
        return view('front.product.detail', compact(['category', 'product']));
    }

    public function search(Request $request)
    {
        $products = Product::where('name', 'LIKE', "%$request->q%")->paginate('10');
        return view('front.product.search', compact('products'));
    }
}
