@extends('layouts.front.template')
@section('metatitle', "$page->title")
@section('metadescription', "$page->title")
@section('title', $page->title)

@section('content')
<section class="blog-posts-area pt-120 mt-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 post-list blog-post-list">
				<div class="single-post">
					<a href="#">
						<h2 class="pb-10">
							{{ $page->title }}
						</h2>
					</a>
          <hr>
					<div class="content-wrap">

						{!! $page->content !!}

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
