<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MenuRequest;
use Yajra\DataTables\Html\Builder;

use App\Models\Menu;
use Ramsey\Uuid\Uuid;
use DataTables;
use Alert;

class MenuController extends Controller
{

		public function index(Builder $builder)
		{
				if (request()->ajax()) {
	          return DataTables::of(Menu::all())
	              ->addColumn('link', function ($menu) {
	                  return '
	                  <div class="btn-group">
										<button onclick="show(this)" data-href="'. route('admin.menu.show', $menu) .'" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></button>
										<a href="'. route('admin.menu.edit', $menu) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
										<button type="button" onclick="konfirm(this)" data-id="'.$menu->id.'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
	                  </div>
	                  ';
	              })
								->addColumn('icon', function ($menu) {
	                  return '
	                  <i class="fa '.$menu->icon.' fa-lg"></i>
	                  ';
	              })
	              ->rawColumns(['link', 'icon'])
	              ->addIndexColumn()
	              ->make(true);
	      }

	      $html = $builder->columns([
	          ['data' => 'name', 'name' => 'name', 'title' => 'Nama', 'searchable'=> true],
	          ['data' => 'url', 'name' => 'url', 'title' => 'URL', 'searchable'=> true],
	          ['data' => 'icon', 'name' => 'icon', 'title' => 'Icon', 'searchable'=> false],
						['data' => 'admin', 'name' => 'admin', 'title' => 'Admin', 'searchable'=> true],
						['data' => 'active', 'name' => 'active', 'title' => 'Active', 'searchable'=> true],
	          ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
	      ])->parameters([
	          'stateSave' => 'true',
	          'stateSaveCallback',
						'sPaginationType' => 'full_numbers'
	      ]);

	      return view('admin.menu.index', compact('html'));
		}

    public function create()
    {
				$parent_menus = Menu::where('active', '=', 'Y')->get();
				return view('admin.menu.create', compact('parent_menus'));
    }

    public function store(MenuRequest $request)
    {
			$data['id_parent'] 	= $request->id_parent;
			$data['name'] 			= $request->name;
			$data['url'] 				= $request->url;
			$data['icon'] 			= $request->icon;
			$data['admin'] 			= $request->admin;
			$data['active'] 		= $request->active;

			if (Menu::insert($data)) {
					Alert::success('Data berhasil disimpan', 'Sukses');
					return redirect()->route('admin.menu.index');
			 }else{
					Alert::error('Silahkan di coba lagi !', 'Error');
					return redirect()->route('admin.menu.create');
			 }
    }

    public function edit(Menu $menu)
    {
			$parent_menus = Menu::select('id', 'name', 'active', 'icon')
									->where('active', '=', 'Y')
									->get();
			return view('admin.menu.edit', compact('menu', 'parent_menus'));
    }

    public function update(MenuRequest $request, Menu $menu)
    {
				$menu->id_parent 	= $request->id_parent;
				$menu->name 			= $request->name;
				$menu->url 				= $request->url;
				$menu->icon 			= $request->icon;
				$menu->admin 			= $request->admin;
				$menu->active 		= $request->active;

				if ($menu->save()) {
						Alert::success('Data berhasil disimpan', 'Sukses');
						return redirect()->route('admin.menu.index');
				}else{
						Alert::error('Silahkan di coba lagi !', 'Error');
						return redirect()->route('admin.menu.edit', $menu);
				}
    }

    public function destroy(Menu $menu)
    {
				if ($menu->delete()){
						Alert::success('Data berhasil dihapus', 'Sukses');
						return redirect()->route('admin.menu.index');
				}else{
						Alert::error('Silahkan di coba lagi !', 'Error');
						return redirect()->route('admin.menu.index');
				}
    }
}
