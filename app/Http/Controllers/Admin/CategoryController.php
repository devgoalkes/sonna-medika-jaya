<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;

use App\Models\Category;
use Ramsey\Uuid\Uuid;
use DataTables;
use Session;
use Alert;

class CategoryController extends Controller
{

    public function index(Builder $builder)
    {
      if (request()->ajax()) {
          return DataTables::of(Category::all())
              ->addColumn('link', function ($category) {
                if($category->slug != 'events' && $category->slug != 'products' && $category->slug != 'article' && $category->slug != 'specialty'){
                    return '<div class="btn-group"><a href="'. route('admin.category.edit', $category->id) .'" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a><button type="button" onclick="konfirm(this)" data-id="'. $category->id.'" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></button>
                    </div>';
                }
              })
              ->addColumn('created_at', function ($category) {
                  return tglWaktuIndo($category->created_at);
              })
              ->rawColumns(['link'])
              ->addIndexColumn()
              ->make(true);
      }

      $html = $builder->columns([
          ['data' => 'DT_Row_Index', 'name' => 'id', 'title' => 'No', 'searchable'=> false, 'orderable' => false],
          ['data' => 'name', 'name' => 'name', 'title' => 'Nama Kategori', 'searchable'=> true],
          ['data' => 'slug', 'name' => 'slug', 'title' => 'Slug', 'searchable'=> true],
          ['data' => 'active', 'name' => 'active', 'title' => 'Aktif', 'searchable'=> false],
          ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Date', 'searchable'=> false],
          ['data' => 'link', 'name' => 'link', 'title' => 'Option', 'orderable'=> false, 'searchable' => false],
      ])->parameters([
          'stateSave' => 'true',
          'stateSaveCallback',
          'sPaginationType' => 'full_numbers'
      ]);

      return view('admin.category.index', compact('html'));
    }

    public function create()
    {
        $parent_menus = Category::where('active', '=', 'Y')->get();
        return view('admin.category.create', compact('parent_menus'));
    }

    public function store(CategoryStoreRequest $request)
    {
        if($request->picture){
          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/category'), $photoName);
        } else {
          $photoName = '';
        }


        Category::insert([
          'id'             => Uuid::uuid4()->toString(),
          'parent_id'      => $request->parent_id,
          'name'           => $request->name,
          'slug'           => str_slug($request->name),
          'active'         => $request->status,
          'picture'        => $photoName,
          'created_at'     => date('Y-m-d H:i:s'),
        ]);

        Alert::success('Data berhasil ditambah !', 'Sukses');
        return redirect()->route('admin.category.index');
    }

    public function edit($id)
    {
        $category = Category::where('id', $id)->first();
        $parent_menus = Category::where('active', '=', 'Y')->get();
        return view('admin.category.edit', compact('category', 'parent_menus'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::where('id', $id)->first();

        $category->name = $request->name;
        $category->slug = str_slug($request->name);
        $category->active = $request->status;
        $category->updated_at = date('Y-m-d H:i:s');

        if($request->picture){
          $photoName = time().'.'.$request->picture->getClientOriginalExtension();
          $request->picture->move(public_path('storage/uploads/category'), $photoName);
          $category->picture = $photoName;
        }else{
          $category->picture = $request->picture_old;
        }

        if ($category->save()) {
            Alert::success('Data berhasil diubah !', 'Sukses');
            return redirect()->route('admin.category.index');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.category.edit', $category);
         }
    }

    public function destroy($id)
    {
        $category = Category::where('id', $id)->first();
        $category->delete();
    }
}
