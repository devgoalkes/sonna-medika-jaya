@extends('layouts.front.template')
@section('title', '')
@section('metatitle', '')
@section('metadescription', '')

@section('content')
<section class="products-area product-page pt-120">
	<div class="container">
    <div class="row d-flex justify-content-center">
			<div class="col-md-12 pb-40 header-text text-center">
				<h2 class="pb-10">Our Specialties</h2>
			</div>
		</div>

		<div class="row pb-80 pt-20">
			@forelse($specialties as $specialty)
			<div class="col-lg-3">
				<div class="single-offered-service">
					<img class="img-fluid" src="{{ asset('storage/uploads/specialty/'. $specialty->picture) }}" alt="{{ $specialty->name }}">
					<a href="{{ route('front.specialty.detail', [$specialty->category, $specialty]) }}">
						<h4>{{ $specialty->title }}</h4>
					</a>
					<p>
						{!! str_limit(strip_tags($specialty->content), 100) !!}
					</p>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
@endsection
