@extends('layouts.admin.template')
@section('title', 'Users')

@section('breadcrumb')
<div id="breadcrumb">
	<ul class="breadcrumb">
		 <li><i class="fa fa-home"></i><a onclick="location.href='{{ route('admin') }}'"> Home</a></li>
		 <li class="active">User</li>
	</ul>
</div>
@endsection

@section('content')
<div class="panel panel-primary table-responsive">
	<div class="panel-heading">
		Data User
	</div>
	<div class="padding-md clearfix">
		@section('button')
		<a href="{{ route('admin.user.create') }}" class="btn btn-primary btn-small"><i class="fa fa-plus"></i> Add User </a>
		@endsection
		<table class="table table-condensed table-bordered" id="dataTable">
			<thead>
				<tr>
					<th width="20px">No</th>
					<th>Name</th>
					<th>Email</th>
					<th>Created At</th>
					<th width="80px">Action</th>
				</tr>
			</thead>
			<tbody>
				@if(!$users->isEmpty())
				@foreach($users as $user)
				<tr>
					<td>{{ $user->no }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->created_at }}</td>
					<td>
						@if(Auth::user()->id != $user->id)
						<a href="user/edit/{{ $user->id }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
						{!! Form::open(['route' => ['admin.user.destroy', $user->id], 'method' => 'DELETE', 'class' => 'delete_form', 'style' => 'display:inline']) !!}
						<button class="btn btn-danger btn-xs delete_button"><i class="fa fa-trash"></i></button>
						{!! Form::close() !!}
						@endif
					</td>
				</tr>
				@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('style')
<link href="{{ asset('asset_admin/css/jquery.dataTables_themeroller.css') }}" rel="stylesheet">
<link href="{{ asset('asset_admin/css/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ asset('asset_admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('asset_admin/js/sweetalert.min.js') }}"></script>
<script>
	$(function	()	{
			$('#dataTable').dataTable( {
				"bJQueryUI": true,
				"bLengthChange": false,
				"sPaginationType": "full_numbers"
			});
	    $('button.delete_button').on('click', function(e){
	    	e.preventDefault();
	    	var self = $(this);
	    	swal({
	        title: "Alert",
	        text: "Apakah anda yakin ingin menghapus data ini ?",
	        type: "warning",
	        showCancelButton: true,
	        confirmButtonColor: "#cc3f44",
	        confirmButtonText: "Hapus",
	        cancelButtonText : "Batal",
	        closeOnConfirm: false,
	        closeOnCancel: false
	    	},
	    	function(isConfirm) {
		        if(isConfirm){
		        	self.parents(".delete_form").submit();
		        }else{
		        	swal('Cancelled', 'Data anda gagal dihapus !', 'error');
		        }
		    });
	    });
		});
</script>
@endsection
