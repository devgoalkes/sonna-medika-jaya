<head>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') | Administrator {{ config('app.name') }}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Administrator {{ config('app.name') }}">
  <link rel="icon" type="image/gif" href="{{ asset('images/favicon.gif') }}">
  <link href="{{ asset('asset_admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('asset_admin/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('asset_admin/css/chosen/chosen.min.css') }}" rel="stylesheet">
  <link href="{{ asset('asset_admin/css/pace.css') }}" rel="stylesheet">
  <link href="{{ asset('asset_admin/css/morris.css') }}" rel="stylesheet"/>
  <link href="{{ asset('asset_admin/css/app.min.css') }}" rel="stylesheet">
  <link href="{{ asset('asset_admin/css/app-skin.css') }}" rel="stylesheet">
  @yield('style')

</head>
