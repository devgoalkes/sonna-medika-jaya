@extends('layouts.front.template')
@section('title', "502 Bad Gateway")
@section('metatitle', "502 Bad Gateway")
@section('metadescription', "502 Bad Gateway")
@section('content')
<section class="blog-posts-area pt-120 mt-20">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 post-list blog-post-list text-center">
				<div class="single-post">
					<a href="#">
						<h2>502!</h2>
					</a>
          <hr>
					<div class="content-wrap">

            <h5>502 Bad Gateway</h5>
            <p>The server encountered a temporary error and could not complete your request!</p>
						<p class="notfound">Please try again!</p>
            <a href="{{ url('/') }}" class="default-btn">Back to Home</a>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
