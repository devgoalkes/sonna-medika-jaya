<header id="header" id="home">
  <div class="container main-menu">
    <div class="row align-items-center justify-content-between d-flex">
      <div id="logo">
        <a href="/"><img src="{{ asset('front/img/logo 2.png') }}" height="60px" alt="" title="" /></a>
      </div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          @php $menus = \App\Models\Menu::where('id_parent',0)->where('active', 'Y')->where('admin', 'N')->orderBy('id', 'asc')->get(); @endphp

          @foreach($menus as $menu)
              @if($menu->id_parent == 0)
                  @if($menu->url == 'product')
                      @php
                        $product = \App\Models\Category::where('slug', 'products')->where('active', 'Y')->first();
                      @endphp
                      <li class="menu-has-children">
                          <a href="{{ url($menu->url) }}">{{ $menu->name }}</a>
                          <ul>
                              @foreach($product->subcategory as $sub)
                                  @if($sub->subcategory->count() > 1)
                                    <li class="menu-has-children">
                                        <a href="{{ route('front.product.category', $sub) }}">{{ $sub->name }}</a>
                                        <ul>
                                            @foreach($sub->subcategory as $subsub)
                                            <li><a href="{{ route('front.product.category', $subsub) }}">{{ $subsub->name }}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                  @else
                                    <li><a href="{{ route('front.product.category', $sub) }}">{{ $sub->name }}</a></li>
                                  @endif
                              @endforeach
                          </ul>
                      </li>
                  @elseif($menu->url == 'specialty')
                      @php
                        $specialty = \App\Models\Category::where('slug', 'specialty')->where('active', 'Y')->first();
                      @endphp
                      <li class="menu-has-children">
                          <a href="{{ url($menu->url) }}">{{ $menu->name }}</a>
                          <ul>
                              @foreach($specialty->subcategory as $subcat)
                                  @if($subcat->subcategory->count() > 0)
                                      <li class="menu-has-children">
                                          <a href="{{ route('front.specialty.category', $subcat) }}">{{ $subcat->name }}</a>
                                          <ul>
                                              @foreach($subcat->subcategory as $subsubcat)
                                                  <li><a href="{{ route('front.specialty.category', $subsubcat) }}">{{ $subsubcat->name }}</a></li>
                                              @endforeach
                                          </ul>
                                      </li>
                                  @else
                                      <li>
                                          <a href="{{ route('front.specialty.category', $subcat) }}">{{ $subcat->name }}</a>
                                      </li>
                                  @endif
                              @endforeach
                          </ul>
                      </li>
                  @else
                      @if($menu->submenu->count() > 0)
                          <li class="menu-has-children">
                              <a href="{{ url($menu->url) }}">{{ $menu->name }}</a>
                              <ul>
                                  @foreach($menu->submenu as $submenu)
                                  <li><a href="{{ url($submenu->url) }}">{{ $submenu->name }}</a></li>
                                  @endforeach
                              </ul>
                          </li>
                      @else
                          <li><a href="{{ url($menu->url) }}">{{ $menu->name }}</a></li>
                      @endif
                  @endif
              @endif
          @endforeach
        <li><a href="https://www.goalkes.com/sonnamedika" target="_blank"><i class="fa fa-cart-plus fa-lg" style="font-size: 20px;"></i></a></li>
        </ul>
      </nav>
    </div>
  </div>
</header>
