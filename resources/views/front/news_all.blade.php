@extends('layouts.front.template')
@section('metatitle', "Semua Artikel")
@section('metadescription', "Semua Artikel")
@section('front_title', 'Semua Artikel')

@section('content')
<div class="sidebar-page-container">
	<div class="auto-container">
  	<div class="row clearfix">

      <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
      	<div class="blog-classic">

          @foreach ($news as $r)
          <div class="news-block-three">
            <div class="inner-box">
							@if($r->picture != '')
              <div class="image">
                <img src="{{ url('storage/uploads/news/'.$r->picture) }}" alt="{{ $r->title }}" />
                <a class="overlay-link" href="{{ route('artikel.detail', $r->slug) }}"><span class="icon fa fa-link"></span></a>
              </div>
							@endif
              <div class="lower-content">
                <h3><a href="{{ route('artikel.detail', $r->slug) }}">{{ $r->title }}</a></h3>
                <div class="text">{!! str_limit(strip_tags($r->content), 200) !!} [...]</div>
                <ul class="post-meta">
                  <li><span class="icon fa fa-comments-o"></span>{{ $r->comments->count() }} Komentar</li>
                  <li><span class="icon fa fa-user"></span>{{ ucfirst($r->user->name) }}</li>
                  <li><span class="icon fa fa-calendar"></span>{{ tglIndo($r->created_at) }}</li>
                </ul>
              </div>
            </div>
          </div>
          @endforeach

          <ul class="styled-pagination text-center">
            {{ $news->links() }}
          </ul>

        </div>
      </div>

      @include('layouts.front.partials._sidebar')

      </div>
    </div>
  </div>

@endsection

@push('js')
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endpush
