<?php
namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Ramsey\Uuid\Uuid;
use App\Models\{ Help, Category, Article, Setting, Product, Event };

class HomeController extends Controller
{
    public function index(){
        $categories = Category::where('active', 'Y')->get();
        $articles   = Article::get();
        $setting    = Setting::first();
        $products   = Product::where('active', 'Y')->orderBy('created_at', 'desc')->limit(4)->get();
        $events    = Event::where('active', 'Y')->limit(6)->get();
        return view('front.home', compact('categories', 'articles', 'setting', 'products', 'events'));
    }


    public function autoComplete(Request $request) {
        $query = $request->get('term','');
        $news  = Help::where('title','LIKE','%'.$query.'%')->get();
        $data  = array();
        foreach ($news as $r) {
            $data[]=array('title'=>$r->title,'slug'=>$r->id);
        }
        if(count($data))
            return $data;
        else
            return ['title'=>'No Result Found','slug'=>''];
    }
}
