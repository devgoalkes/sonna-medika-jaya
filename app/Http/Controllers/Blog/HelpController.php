<?php

namespace App\Http\Controllers\Blog;
use App\Http\Controllers\Controller;
use App\Models\{ Category, Help, Article };
use Ramsey\Uuid\Uuid;
use Mail;

class HelpController extends Controller
{
    public function index()
    {
      $categories = Category::where('active', 'Y')->get();
      return view('front.help.index', compact('categories'));
    }

    public function category(Category $category)
		{
			$helps = $category->helps()->where('active', 'Y')->orderBy('created_at', 'desc')->paginate(10);
			return view('front.help.category', compact(['helps', 'category']));
		}

    public function show(Category $category, Help $help)
    {
          // $help->update(
          //   [$help->read => $help->read + 1]
          // );

          return view('front.help.show', compact('category', 'help'));

    }

    public function tabShow(Help $help)
    {
          return view('front.help.tab', compact('help'));
    }
}
