<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;

use DataTables;

use App\Models\{ User, UserProfil };
use Ramsey\Uuid\Uuid;
use Session;
use Alert;

class UserController extends Controller
{
    public function index()
    {
        DB::statement(DB::raw('set @nomor=0 '));
        $users = User::select(DB::raw('@nomor := @nomor + 1 as no'), 'id','name', 'email', 'created_at')
            ->orderBy('users.created_at', 'desc')->get();
        return view('admin.user.index', compact('users'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(UserRequest $request)
    {
        $data['id'] = Uuid::uuid4()->toString();
        $data['name'] = $request->name;
        $data['password'] = bcrypt($request->password);
        $data['email'] = $request->email;
        $data['created_at'] = date('Y-m-d H:i:s');

        if (User::insert($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.user.index');
         }else{
            Alert::error('Silahkan coba lagi !', 'Error');
            return redirect()->route('admin.user.create');
         }
    }

    public function edit(User $user)
    {
        return view('admin.user.edit', compact('user'));
    }

    public function update(UserRequest $request, User $user)
    {
        if ($request->password == '') {
            $data['name']       = $request->name;
            $data['email']      = $request->email;
            $data['updated_at'] = date('Y-m-d H:i:s');
        }else {
            $data['name']       = $request->name;
            $data['email']      = $request->email;
            $data['password']   = bcrypt($request->password);
            $data['updated_at'] = date('Y-m-d H:i:s');
        }

        if ($user->update($data)) {
            Alert::success('Data berhasil disimpan', 'Sukses');
            return redirect()->route('admin.user.index');
         }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.user.edit', $user);
         }
    }

    public function destroy(User $user)
    {
        if ($user->delete()){
            Alert::success('Data berhasil dihapus', 'Sukses');
            return redirect()->route('admin.user.index');
        }else{
            Alert::error('Silahkan di coba lagi !', 'Error');
            return redirect()->route('admin.user.index');
        }
    }

    public function edit_profil($id)
    {
        $users = UserModel::where('id', $id)->first();
        $profil = UserProfil::where('user_id', $id)->first();

        if ($profil->count() == 0) {
            return view('admin.user_profil.create', compact('users', 'profil'));
        }else{
            return view('admin.user_profil.edit', compact('users', 'profil'));
        }
    }
}
